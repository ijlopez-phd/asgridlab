
#include <class.h>
#include <timestamp.h>
#include "gridopservicesproxy.h"

#ifndef NULL_SIMULATION_ID
#define NULL_SIMULATION_ID -1
#endif

/**
 * Definition of the variables that are global to the module.
 */
char1024 nodeName = "NON_NODE_NAME";
char256 scenarioCode = "NON_SCENARIO_CODE";
char1024 xmppUrl = "localhost";
int32 idSimulation = NULL_SIMULATION_ID;
int32 maxStepTime = 7200;
int defaultOadrLevel = 0;
bool waitForXmppMessage = false;
int32 defaultPrice = 100;
int32 nDirectories = 1;
float overbookingFactor = 0;
int32 useStartingPrice = 0;
int32 useRandom = 1;
int32 constantF = 0;

