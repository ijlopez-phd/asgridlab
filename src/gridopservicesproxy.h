/*
 * gridopservicesproxy.h
 *
 *  Created on: Jul 18, 2013
 *      Author: ijlopez
 */

#ifndef GRIDOPSERVICESPROXY_H_
#define GRIDOPSERVICESPROXY_H_

#include <timestamp.h>
#include <string>



using namespace std;

typedef struct {
	TIMESTAMP nextPause;
	bool eventTriggered;
} GridopPause;

class GridopServicesProxy {

public:

	/**
	 * Destructor.
	 */
	virtual ~GridopServicesProxy() = 0;


	/**
	 * Tells the Grid Operator that a new simulation is starting.
	 */
	virtual int simulationStarted() = 0;


	/**
	 * Tells the Grid Operator that the simulation is paused in order for
	 * the elements can process their new state.
	 */
	virtual GridopPause simulationPaused(TIMESTAMP modelTimestamp, TIMESTAMP realTimestamp) = 0;


	/**
	 * Tells the Grid Operator that the simulation is over.
	 */
	virtual void simulationFinished() = 0;


	/**
	 * Tells the Grid Operator to run a new ASPEM server.
	 */
	virtual void createASPEM(string name, string longName, string host, int port) = 0;

};


class GridopRestfulProxy : public GridopServicesProxy {

public:

	GridopRestfulProxy(string wsEndpoint, string scenarioCode);

	virtual ~GridopRestfulProxy();

	virtual int simulationStarted();

	virtual GridopPause simulationPaused(TIMESTAMP modelTimestamp, TIMESTAMP realTimestamp);

	virtual void simulationFinished();

	virtual void createASPEM(string name, string longName, string host, int port);

private:

	int idSimulation;
	string wsEndpoint;
	string scenarioCode;
};


class GridopMockProxy : public GridopServicesProxy {

public:

	GridopMockProxy() {
		return;
	}

	virtual ~GridopMockProxy() {
		return;
	}

	virtual int simulationStarted() {
		return 1;
	}

	virtual GridopPause simulationPaused(TIMESTAMP modelTimestamp, TIMESTAMP realTimestamp) {
		GridopPause p;
		p.nextPause = TS_NEVER;
		p.eventTriggered = false;
		return p;
	}

	virtual void simulationFinished() {
		return;
	}

	virtual void createASPEM(string name, string longName, string host, int port) {
		return;
	}
};


class GridopServicesProxyFactory {

public:

	static GridopServicesProxy* getProxy(string wsEndpoint, string scenarioCode);

};


#endif /* GRIDOPSERVICESPROXY_H_ */
