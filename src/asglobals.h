
#ifndef AS_GLOBALS_H_
#define AS_GLOBALS_H_

#include <class.h>
#include <timestamp.h>
#include "gridopservicesproxy.h"

#define NULL_SIMULATION_ID -1

extern char1024 nodeName; /** name of the simulated node. */
extern char256 scenarioCode; /** code of the scenario to be simulated. */
extern char1024 xmppUrl; /** Url to connect with the XMPP server (host:port). */
extern int32 idSimulation; /** Identifier of the simulation. */
extern int32 maxStepTime; /** Maximum simulation time (secs) without calling the Grid Operator. */
extern bool waitForXmppMessage; /** Determine if ASBoxs have to wait for a message. */
extern int defaultOadrLevel; /** Level of the global DR signal to be applied (0, 1, 2, 3). */
extern int defaultPrice; /** Default price at which the brokers are willing to adopt a specific level. */
extern int32 nDirectories; /** Number of Directory facilitators to use in the simulation. */
extern float overbookingFactor; /** Overbooking factor to use in the simulation. */
extern int32 useStartingPrice; /** Tells whether the starting price must be used in simulations. */
extern int32 useRandom; /** Tells if buyers should order or randomized the sellers. */
extern int32 constantF; /** Set the constant F. Level of competetiveness on the buyers' side. */

#endif /* GLOBALS_H_ */
