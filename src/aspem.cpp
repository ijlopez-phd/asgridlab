
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <sstream>
#include <wonrest/wonrest.h>
#include "aspem.h"
#include "asutils.h"
#include "gridop.h"



using namespace wonrest;
using namespace std;

CLASS* ASPEM::oclass = NULL;
ASPEM *ASPEM::defaults = NULL;

// TODOIGN: remove passes that aren't neeeded
static PASSCONFIG passconfig = PC_PRETOPDOWN|PC_BOTTOMUP|PC_POSTTOPDOWN;

// TODOIGN: specify which pass the clock advances
static PASSCONFIG clockpass = PC_BOTTOMUP;

// Forward declaration of static methods (file methods).
AspemPath* getAspemPath(string wsEndpoint);


// -------------------------------------------------------------------------------------
ASPEM::~ASPEM() {

	if (this->wsProxy != NULL) {
		delete this->wsProxy;
	}

	return;
}

// -------------------------------------------------------------------------------------
ASPEM::ASPEM(MODULE *module) : wsProxy(NULL) {

	if (oclass != NULL) {
		gl_warning("ASPEM: This object is already registered.");
		return;
	}

	// Register the class with the module
	oclass = gl_register_class(module, "ASPEM", sizeof(ASPEM), passconfig);
	if (oclass == NULL) {
		GL_THROW("unable to register object class implemented by %s", __FILE__);
		return;
	}

	// Publish the class properties
	if (gl_publish_variable(
			oclass,
			PT_char1024, "longName", PADDR(longName), PT_DESCRIPTION, "ASPEM more descriptive name.",
			PT_char1024, "servicesEndpoint", PADDR(wsEndpoint), PT_DESCRIPTION, "Url of the services layer of the ASPEM.",
			NULL) < 1) {
		GL_THROW("unable to publish properties in %s", __FILE__);
	}

	return;
}


// -------------------------------------------------------------------------------------
AspemServicesProxy* ASPEM::getServicesProxy() const {
	return this->wsProxy;
}


// -------------------------------------------------------------------------------------
const char* ASPEM::getLongName() const {
	return this->longName;
}


// -------------------------------------------------------------------------------------
void ASPEM::finish() {
	this->getServicesProxy()->simulationFinished();
	return;
}


// -------------------------------------------------------------------------------------
AspemPath* getAspemPath(string wsEndpoint) {

	AspemPath* ap = new AspemPath;

	// Split the path "Web Services Endpoint".
	// The fist token defines the host and the port.
	vector<string> urlTokens;
	urlTokens = AsUtils::split(wsEndpoint, '/', urlTokens);
	if (urlTokens[0].find_first_of(':') != string::npos) { // the port is specified in the path
		vector<string> hostTokens;
		hostTokens = AsUtils::split(urlTokens[0], ':', hostTokens);
		if (hostTokens.size() != 2) {
			gl_error("Aspem: getAspemPath: the syntax of the endpoint is not well-formed.");
			throw;
		}

		ap->host = hostTokens[0];
		ap->port = atoi(hostTokens[1].c_str());

	} else { // the port is NOT specified in the path. It is assumed that is 80.
		ap->host = urlTokens[0];
		ap->port = 80;
	}

	return ap;
}


/**
 * -------------------------------------------------------------------------------------------
 * The create function is run before user-defined values are loaded and "init" is performed.
 * This event allows you to set up each "object" prior to user values being set by the GLM file.
 * This is the time to set default values and values used to detect whether the user defined
 * required values (e.g., negative or zero that are not valid).
 *
 * The "create" function must return SUCCESS or FAILED to indicate the result. The simulation
 * will stop if the return value is not SUCCESS.
 *
 */
int ASPEM::create() {
	this->longName[0] = '\0';
	this->wsEndpoint[0] = '\0';

	return SUCCESS;
}


/**
 * -------------------------------------------------------------------------------------------
 * Object initialization is called once after all object have been created.
 * The values specified by the user at the GLM file are loaded at this time.
 *
 */
int ASPEM::init(OBJECT *parent) {

	// Header data of the ASPEM.
	OBJECT *hdr = OBJECTHDR(this);

	// Check that the ASPEM has been declared as child of the Grid Operator object.
	if ((parent == NULL) || (strcmp(parent->oclass->name, "GridOperator") != 0))  {
		gl_error("ASPEM: %s: init: The ASPEM must be declared as child of the Grid Operator object.", hdr->name);
		return FAILED;
	}

	// Tell the GridOperator that a new ASPEM must be instantiated.
	GridOperator* gridop = OBJECTDATA(parent, GridOperator);
	AspemPath *ap = getAspemPath(this->wsEndpoint);
	gridop->getServicesProxy()->createASPEM(hdr->name, longName, ap->host, ap->port);
	delete ap;

	// Tell the ASPEM that the simulation has been initiated.
	this->wsProxy = AspemServicesProxyFactory::getProxy(this->wsEndpoint, idSimulation);
	this->wsProxy->simulationStarted();

	return SUCCESS;
}


// -------------------------------------------------------------------------------------
TIMESTAMP ASPEM::presync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2 = TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


// -------------------------------------------------------------------------------------
TIMESTAMP ASPEM::sync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2 = TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


// -------------------------------------------------------------------------------------
TIMESTAMP ASPEM::postsync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2= TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure
}


//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF CORE LINKAGE
//////////////////////////////////////////////////////////////////////////

EXPORT int create_ASPEM(OBJECT **obj)
{
	try
	{
		*obj = gl_create_object(ASPEM::oclass);
		if (*obj!=NULL)
			return OBJECTDATA(*obj,ASPEM)->create();
	}
	catch (char *msg)
	{
		gl_error("create_ASPEM: %s", msg);
	}
	return 0;
}

EXPORT int init_ASPEM(OBJECT *obj, OBJECT *parent)
{
	try
	{
		if (obj!=NULL) {
			return OBJECTDATA(obj, ASPEM)->init(parent);
		}
	} catch (char *msg)	{
		gl_error("init_ASPEM(obj=%d;%s): %s", obj->id, obj->name?obj->name:"unnamed", msg);
	}
	return 0;
}

EXPORT TIMESTAMP sync_ASPEM(OBJECT *obj, TIMESTAMP t1, PASSCONFIG pass)
{
	TIMESTAMP t2 = TS_NEVER;
	ASPEM *my = OBJECTDATA(obj,ASPEM);
	try
	{
		switch (pass) {
		case PC_PRETOPDOWN:
			t2 = my->presync(obj->clock,t1);
			break;
		case PC_BOTTOMUP:
			t2 = my->sync(obj->clock,t1);
			break;
		case PC_POSTTOPDOWN:
			t2 = my->postsync(obj->clock,t1);
			break;
		default:
			GL_THROW("invalid pass request (%d)", pass);
			break;
		}
		if (pass==clockpass)
			obj->clock = t1;
		return t2;
	}
	catch (char *msg)
	{
		gl_error("sync_ASPEM(obj=%d;%s): %s", obj->id, obj->name?obj->name:"unnamed", msg);
	}
	return TS_INVALID;
}
