
#include "xmppbot.h"
#include <iostream>
#include <sstream>
#include <gloox/jid.h>
#include <gloox/presence.h>
#include <gridlabd.h>
#include "asglobals.h"

using namespace std;

/**
 *
 * XmppBot.
 *
 */

// ---------------------------------------------------------------------------------------
XmppBot::XmppBot(string serverUrl, string nodeName)
		: serverUrl(serverUrl), nodeName(nodeName), client(NULL), connected(false),
		  hasReceivedMessage(false) {

	accountName = createAccountName(nodeName);
	accountPassword = createAccountPassword(nodeName);
	brokerAccountName = getBrokerAccountName(nodeName);
	return;
}


// ---------------------------------------------------------------------------------------
XmppBot::~XmppBot() {

	if (this->client != NULL) {
		client->disconnect();
		delete client;
		client = NULL;
	}

	return;
}


// ---------------------------------------------------------------------------------------
string XmppBot::receiveMessage() {
	this->hasReceivedMessage = false;
	while (!this->hasReceivedMessage) {
		client->recv();
	}

	return this->lastMessage;
}


// ---------------------------------------------------------------------------------------
void XmppBot::disconnect() {

	if (client == NULL) {
		return;
	}

	// Remove the account.
	gloox::Registration *reg = new gloox::Registration(client);
	reg->removeAccount();
	delete reg;

	// Disconnect from the XMPP server.
	client->disconnect();
	while (connected) {
		client->recv(100000);
	}
	delete client;
	client = NULL;

	return;
}


// ---------------------------------------------------------------------------------------
void XmppBot::connect() {

	if (client != NULL) {
		return;
	}

	// Register the XMPP account with the server.
	if (registerAccount() == false) {
		// Failed to create the new account.
		throw Err_Registration;
	}

	// Connect to the XMPP server using the recently created account.
	gloox::JID jid;
	jid.setUsername(accountName);
	jid.setServer(serverUrl);
	jid.setResource("energyagents");
	client = new gloox::Client(jid, accountPassword);
	client->registerConnectionListener(this);
	client->registerMessageHandler(this);
	//client->logInstance().registerLogHandler(gloox::LogLevelDebug, gloox::LogAreaAll, this);
	if (!client->connect(false)) {
		// Failed to connect to the XMPP server.
		delete client;
		client = NULL;
		throw Err_Connection;
	}

	// Receive the messages from the XMPP server until the connection is established.
	while (!connected) {
		client->recv(100000);
	}

	return;
}


// ---------------------------------------------------------------------------------------
bool XmppBot::registerAccount() {

	gloox::Client *regClient = new gloox::Client(serverUrl);
	regClient->disableRoster(); // roster is not necessary for registration.
	regClient->setSASLMechanisms(gloox::SaslMechAll ^ gloox::SaslMechAnonymous);
	//regClient->logInstance().registerLogHandler(gloox::LogLevelDebug, gloox::LogAreaAll, this);
	if (!regClient->connect(false)) {
		gl_error("Failed to connect to the XMPP server in order to create a new account.");
		delete regClient;
		return false;
	}

	gloox::Registration *reg = new gloox::Registration(regClient);
	XmppBotRegistrationHandler *regHandler = new XmppBotRegistrationHandler();
	reg->registerRegistrationHandler(regHandler);
	//reg->fetchRegistrationFields();

	struct gloox::RegistrationFields fieldsData;
	fieldsData.username = accountName;
	fieldsData.password = accountPassword;
	fieldsData.name = accountName;
	fieldsData.email = "none";
	fieldsData.nick = accountName;
	fieldsData.first = "none";
	fieldsData.last = "none";
	bool accountCreated = reg->createAccount(
			gloox::Registration::FieldUsername | gloox::Registration::FieldPassword |
			gloox::Registration::FieldName | gloox::Registration::FieldEmail,
			fieldsData);

	gloox::RegistrationResult result = regHandler->getResult();
	bool isAlreadyRegistered = regHandler->isAlreadyRegistered();

	delete reg;
	delete regClient;

	if ((accountCreated == true) || (isAlreadyRegistered == true)) {
		return true;
	}

	gl_error("Failed to create a new account in the XMPP server: %d\n", result);

	return false;
}


// ---------------------------------------------------------------------------------------
void XmppBot::onConnect() {
	this->connected = true;
	return;
}


// ---------------------------------------------------------------------------------------
void XmppBot::onDisconnect(gloox::ConnectionError e) {
	this->connected = false;
	return;
}


// ---------------------------------------------------------------------------------------
bool XmppBot::onTLSConnect(const gloox::CertInfo& info) {
	return true;
}


// ---------------------------------------------------------------------------------------
void XmppBot::handleMessage(const gloox::Message &msg, gloox::MessageSession *session) {
	this->hasReceivedMessage = true;
	this->lastMessage = msg.body();
	return;
}


// -------------------------------------------------------------------------------------
string XmppBot::createAccountName(const string &nodeName) const {
	stringstream accountName;
	accountName << "la-" << nodeName;
	return accountName.str();
}


// -------------------------------------------------------------------------------------
string XmppBot::createAccountPassword(const string &nodeName) const {
	return createAccountName(nodeName);
}


// -------------------------------------------------------------------------------------
string XmppBot::getBrokerAccountName(const string &nodeName) const {
	stringstream accountName;
	accountName << "ba-" << nodeName;
	return accountName.str();
}


// -------------------------------------------------------------------------------------
void XmppBot::handleLog(gloox::LogLevel level, gloox::LogArea, const std::string &message) {
	cout << "XmppBot:" << message << endl;
	return;
}
