
#ifndef _AS_XMPPBOT_H_
#define _AS_XMPPBOT_H_

#include <string>
#include <vector>
#include <gloox/gloox.h>
#include <gloox/message.h>
#include <gloox/messagehandler.h>
#include <gloox/client.h>
#include <gloox/connectionlistener.h>
#include <gloox/registration.h>
#include <gloox/registrationhandler.h>
#include <gloox/loghandler.h>
#include <gloox/stanzaextension.h>
#include <gloox/tag.h>

using namespace std;

class XmppBot;
class XmppBotRegistrationHandler;

/**
 * Class XmppBot.
 * Connect to the XMPP server and handles the main events.
 */
class XmppBot: protected gloox::ConnectionListener,
		protected gloox::LogHandler,
		protected gloox::MessageHandler {

public:

	enum {
		Err_Registration = -1,
		Err_Connection = -2
	};

	XmppBot(string serverUrl, string nodeName);

	~XmppBot();

	void connect();

	void disconnect();

	string receiveMessage();

protected:

	virtual void onConnect();

	virtual void onDisconnect(gloox::ConnectionError e);

	virtual bool onTLSConnect( const gloox::CertInfo& info );

	virtual void handleLog(gloox::LogLevel level, gloox::LogArea area, const std::string &message);

	virtual void handleMessage(const gloox::Message &msg, gloox::MessageSession *session=0);

private:

	string createAccountName(const string &nodeName) const;

	string createAccountPassword(const string &nodeName) const;

	string getBrokerAccountName(const string &nodeName) const;

	bool registerAccount();

	string serverUrl;

	string nodeName;

	string accountName;

	string accountPassword;

	string brokerAccountName;

	bool connected;

	bool hasReceivedMessage;

	string lastMessage;

	gloox::Client* client;
};


/**
 * Class XmppBotRegistrationHandler.
 * Handle the events related to the creation of a new account.
 */
class XmppBotRegistrationHandler : public gloox::RegistrationHandler {

public:

	XmppBotRegistrationHandler()
		: alreadyRegistered(false), result(gloox::RegistrationUnknownError) {};

	virtual ~XmppBotRegistrationHandler() {};

	virtual void handleRegistrationFields(const gloox::JID &from, int fields, string instructions) {}

	virtual void handleAlreadyRegistered(const gloox::JID &from) {
		this->alreadyRegistered = true;
	}

	virtual void handleRegistrationResult(const gloox::JID &from, gloox::RegistrationResult regResult) {
		this->result = regResult;
		return;
	}

	virtual void handleDataForm(const gloox::JID &from, const gloox::DataForm &form) {}

	virtual void handleOOB(const gloox::JID &from, const gloox::OOB &oob) {}

	bool isAlreadyRegistered() {
		return this->alreadyRegistered;
	}

	gloox::RegistrationResult getResult() {
		return this->result;
	}

private:

	bool alreadyRegistered;

	gloox::RegistrationResult result;


};
#endif
