#ifndef DRCOMMANDER_H_
#define DRCOMMANDER_H_

#include <string>
#include <config.h>
#include <house_e.h>
#include <waterheater.h>
#include <zipload.h>


using namespace std;

/**
 * Stores the initial set points of the HVAC.
 */
typedef struct {
	double heatingSetpoint;
	double coolingSetpoint;
} HvacStatus;


/**
 * Stores the initial set points of the water heater.
 */
typedef struct {
	double setpoint;
} WaterHeaterStatus;


/**
 * Class AsdrCommander.
 * It is the responsible for executing and stopping demand-response commands according to the
 * events received by the AS-Box.
 */

class AsdrCommander {

public:

	/** Constructor. */
	AsdrCommander();

	/** Destructor. */
	virtual ~AsdrCommander();

	/** Execute a DR command in response to a DR event. */
	virtual void commit(float payload) = 0;

	/** Stop the execution of a DR command. */
	virtual void stop() = 0;
};


/**
 * Class BasicAsdrCommander.
 * It applies commands in response to simple events. Events can only contain signals of the type
 * 'level'.
 *
 */
class BasicAsdrCommander : public AsdrCommander {

public:

	/** Constructor. */
	BasicAsdrCommander(house_e* house);

	/** Destructor. */
	virtual ~BasicAsdrCommander();

	/** Execute a DR command in response to a DR event. */
	virtual void commit(float payload);

	/** Stop the execution of a DR command. */
	virtual void stop();

private:

	static const int LEVEL_NORMAL = 0;
	static const int LEVEL_MODERATE = 1;
	static const int LEVEL_HIGH = 2;
	static const int LEVEL_CRITICAL = 3;

	static const double DELTA_WATER_HEATER_MODERATE = 4;
	static const double DELTA_WATER_HEATER_HIGH = 8;

	static const double COOLER_SETPOINT_MODERATE = 75.2; // 24 degrees celcius.
	static const double COOLER_SETPOINT_HIGH = 78.8; // 26 degrees celcius.
	static const double HEATER_SETPOINT_MODERATE = 62.5; // 18.5 degrees celcius.
	static const double HEATER_SETPOINT_HIGH = 59; // 15 degress celcius.

	house_e* house; /** House on which the commands are applied. */
	waterheater* waterHeater;
	OBJECT** zipLoads;
	int nZipLoads;


	HvacStatus hvacDefaultStatus; /** Default values of the HVAC. */
	WaterHeaterStatus waterHeaterDefaultStatus; /** Default values of the water heater. */
};


/**
 * Class Commander02.
 */
class AsdrCommander02 : public AsdrCommander {
public:

	/** Constructor. */
	AsdrCommander02(house_e* house);

	/** Destructor. */
	virtual ~AsdrCommander02();

	/** Execute a DR command in response to a DR event. */
	virtual void commit(float payload);

	/** Stop the execution of a DR command. */
	virtual void stop();

private:

	static const int LEVEL_NORMAL = 0;
	static const int LEVEL_MODERATE = 1;
	static const int LEVEL_HIGH = 2;
	static const int LEVEL_CRITICAL = 3;

	static const double COOLER_SETPOINT_MODERATE = 75.2; // 24 degrees celcius.
	static const double COOLER_SETPOINT_HIGH = 78.8; // 26 degrees celcius.
	static const double HEATER_SETPOINT_MODERATE = 62.5; // 18.5 degrees celcius.
	static const double HEATER_SETPOINT_HIGH = 59; // 15 degress celcius.

	house_e* house; /** House on which the commands are applied. */
	waterheater* waterHeater;
	OBJECT** zipLoads;
	int nZipLoads;


	HvacStatus hvacDefaultStatus; /** Default values of the HVAC. */
	double defaultDeadband; /** Default deadband of the thermostat. */

};



/**
 * Class MockAsdrCommander.
 * It does not apply any command in response to the DR events.
 *
 */
class MockAsdrCommander : public AsdrCommander {

public:

	/** Constructor. */
	MockAsdrCommander() {};

	/** Destructor. */
	virtual ~MockAsdrCommander() {};

	/** Execute a DR command in response to a DR event. */
	virtual void commit(float payload) {};

	virtual void stop() {};
};



/**
 * Class AsdrCommanderFactory.
 *
 */
class AsdrCommanderFactory {
public:

	static AsdrCommander* getCommander(house_e* house);
};


#endif /* DRCOMMANDER_H_ */
