#ifndef _ASBOX_H_
#define _ASBOX_H_

#include <string>
#include "xmppbot.h"
#include "agencyservices.h"
#include <gridlabd.h>
#include <config.h>
#include <house_e.h>
#include "distributeevent.h"
#include "commander.h"

using namespace std;

class ASBox {

public:

	// GridLAB-D standard functions
	ASBox(MODULE *module);
	~ASBox();

	int init(OBJECT *parent);
	int create();
	TIMESTAMP presync(TIMESTAMP t0, TIMESTAMP t1);
	TIMESTAMP sync(TIMESTAMP t0, TIMESTAMP t1);
	TIMESTAMP postsync(TIMESTAMP t0, TIMESTAMP t1);

	bool receiveDistributeAction(TIMESTAMP t0, TIMESTAMP t1);

	void finish();

public:
	static CLASS *oclass;
	static ASBox *defaults;
	// static CLASS *plcass; // parent class
	// TIMESTAMP plc(TIMESTAMP t0, TIMESTAMP t1); /**< defines the default PLC code */

private:

	house_e* house;
	char1024 xmppAddress;
	char256 levelsSchedule;
	int32 priority;
	int32 easyLevel;
	int32 hardLevel;
	char32 loadMode;
	int32 levelPrice1;
	int32 levelPrice2;
	int32 levelPrice3;
	int32 startingPrice;
	object aspem;
	XmppBot* xmppBot;
	DistributeEvent *distAction;
	AsdrCommander *commander;

};


class UserPreferences {

public:

	UserPreferences();

	~UserPreferences();

	void setPriority(int32 priority);

	int32 getPriority() const;

	void setEasyLevel(int32 easyLevel);

	int32 getEasyLevel() const;

	void setHardLevel(int32 hardLevel);

	int32 getHardLevel() const;

	void setLoadMode(char* loadMode);

	char* getLoadMode() const;

	void setLevelPrice1(int32 price);

	int32 getLevelPrice1() const;

	void setLevelPrice2(int32 price);

	int32 getLevelPrice2() const;

	void setLevelPrice3(int32 price);

	int32 getLevelPrice3() const;

	void setStartingPrice(int32 startingPrice);

	int32 getStartingPrice() const;

	void setLevelsSchedule(char* levelsScheduleName, char* levelsScheduleDefinition);

	char* getLevelsScheduleName() const;

	char* getLevelsScheduleDefinition() const;

	string toXmlString() const;

private:

	int32 priority;
	int32 easyLevel;
	int32 hardLevel;
	int32 levelPrice1;
	int32 levelPrice2;
	int32 levelPrice3;
	int32 startingPrice;
	char* loadMode;
	char* levelsScheduleName;
	char* levelsScheduleDefinition;

};


#endif /* _ASBOX_H_ */
