#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <wonrest/wonrest.h>
#include <wonrest/restresponse.h>
#include <timestamp.h>
#include "asbox.h"
#include "aspem.h"

#define NO_LEVEL -1
#define NO_PRICE -1
#define NONE_MODE "none"
#define EASY_MODE "easy"
#define HARD_MODE "hard"




CLASS* ASBox::oclass = NULL;
ASBox *ASBox::defaults = NULL;

// TODO: remove passes that aren't neeeded
static PASSCONFIG passconfig = PC_PRETOPDOWN|PC_BOTTOMUP|PC_POSTTOPDOWN;

// TODO: specify which pass the clock advances
static PASSCONFIG clockpass = PC_BOTTOMUP;

using namespace wonrest;
using namespace std;


// -------------------------------------------------------------------------------------
void ASBox::finish() {
	if (xmppBot != NULL) {
		xmppBot->disconnect();
	}

	return;
}


// -------------------------------------------------------------------------------------
ASBox::~ASBox() {
	if (xmppBot != NULL) {
		delete xmppBot;
	}

	if (distAction != NULL) {
		delete distAction;
	}

	return;
}


// -------------------------------------------------------------------------------------
ASBox::ASBox(MODULE *module)  {

	if (oclass != NULL) {
		gl_warning("ASBox: This object is already registered.");
		return;
	}

	// Register the class with the module
	oclass = gl_register_class(module, "ASBox", sizeof(ASBox), passconfig);
	if (oclass == NULL) {
		GL_THROW("unable to register object class implemented by %s", __FILE__);
		return;
	}

	this->xmppBot = NULL;
	this->distAction = NULL;
	this->house = NULL;
	this->commander = NULL;
	this->priority = -1;
	this->levelsSchedule[0] = '\0';
	this->easyLevel = NO_LEVEL;
	this->hardLevel = NO_LEVEL;
	this->loadMode[0] = '\0';
	this->levelPrice1 = defaultPrice;
	this->levelPrice2 = defaultPrice;
	this->levelPrice3 = defaultPrice;
	this->startingPrice = 0;

	// Publish the class properties
	if (gl_publish_variable(
			oclass,
			PT_object, "aspem", PADDR(aspem), PT_DESCRIPTION, "ASPEM to which is attached the AS-Box.",
			PT_int32, "priority", PADDR(priority), PT_DESCRIPTION, "Level of priority contracted by the user",
			PT_int32, "levelEasy", PADDR(easyLevel), PT_DESCRIPTION, "Level of load that is declared as easy-load.",
			PT_int32, "levelHard", PADDR(hardLevel), PT_DESCRIPTION, "Level of load that is declared as hard-load.",
			PT_char32, "loadMode", PADDR(loadMode), PT_DESCRIPTION, "Model of contract (none, easy or hard).",
			PT_char256, "levelsSchedule", PADDR(levelsSchedule), PT_DESCRIPTION, "Name of the Schedule that describes the levels that may accept the client.",
			PT_int32, "levelPrice1", PADDR(levelPrice1), PT_DESCRIPTION, "Price at which the broker is willing to adopt the level 1.",
			PT_int32, "levelPrice2", PADDR(levelPrice2), PT_DESCRIPTION, "Price at which the broker is willing to adopt the level 2.",
			PT_int32, "levelPrice3", PADDR(levelPrice3), PT_DESCRIPTION, "Price at which the broker is willing to adopt the level 3.",
			PT_int32, "startingPrice", PADDR(startingPrice), PT_DESCRIPTION, "Maximum price that a consumer is willing to pay for its energy.",
			NULL) < 1) {
		GL_THROW("unable to publish properties in %s", __FILE__);
	}

	return;
}


/**
 * -------------------------------------------------------------------------------------------
 * The create function is run before user-defined values are loaded and "init" is performed.
 * This event allows you to set up each "object" prior to user values being set by the GLM file.
 * This is the time to set default values and values used to detect whether the user defined
 * required values (e.g., negative or zero that are not valid).
 *
 * The "create" function must return SUCCESS or FAILED to indicate the result. The simulation
 * will stop if the return value is not SUCCESS.
 *
 */
int ASBox::create() {
	this->xmppAddress[0] = '\0';
	this->aspem = NULL;
	this->priority = 0;
	this->levelsSchedule[0] = '\0';
	this->easyLevel = NO_LEVEL;
	this->hardLevel = NO_LEVEL;
	this->loadMode[0] = '\0';
	this->levelPrice1 = defaultPrice;
	this->levelPrice2 = defaultPrice;
	this->levelPrice3 = defaultPrice;
	this->startingPrice = 0;

	return SUCCESS;
}


/**
 * -------------------------------------------------------------------------------------------
 * Object initialization is called once after all object have been created.
 * The values specified by the user at the GLM file are loaded at this time.
 *
 */
int ASBox::init(OBJECT *parent) {

	// Get the header of this ASBox.
	OBJECT *hdr = OBJECTHDR(this);

	// Save the point to which the ASBox is attached.
	if (hdr->parent == NULL) {
		gl_error("AsBox: %s: init: None parent has been defined for this ASBox.", hdr->name);
		return FAILED;
	} else {
		this->house = OBJECTDATA(hdr->parent, house_e);
		if (this->house == NULL) {
			gl_error("Asbox: %s: init: Failed to get the house_e to which the ASBox is attached.", hdr->name);
			return FAILED;
		}
	}

	// Init the Commander (responsible for applying demand-response commands from DR events).
	try {
		this->commander = AsdrCommanderFactory::getCommander(this->house);
	} catch (...) {
		gl_error("ASBox: %s: init: Failed to connect to the XMPP server.", hdr->name);
		return FAILED;
	}

	// Get the data object of the ASPEM.
	ASPEM *aspemData = OBJECTDATA(aspem, ASPEM);

	// Register the AS-Box to the ASPEM
	UserPreferences userPrefs;
	if (strlen(this->levelsSchedule) > 0) {
		// Find the definition of the specified schedule.
		SCHEDULE *sch = callback->schedule.find(this->levelsSchedule);
		if (sch != NULL) {
			userPrefs.setLevelsSchedule(sch->name, sch->definition);
		} else {
			gl_warning("ASBox: %s: The schedule '%s' was not found. No scheduled being used.", hdr->name, this->levelsSchedule);
		}
	}
	userPrefs.setPriority(this->priority);
	userPrefs.setLevelPrice1(this->levelPrice1);
	userPrefs.setLevelPrice2(this->levelPrice2);
	userPrefs.setLevelPrice3(this->levelPrice3);
	userPrefs.setStartingPrice(this->startingPrice);
	aspemData->getServicesProxy()->addAsBox(hdr->name, &userPrefs);

	// Connect to the XMPP server.
	xmppBot = new XmppBot(xmppUrl, hdr->name);
	try {
		this->xmppBot->connect();
	} catch (...) {
		gl_error("ASBox: %s: init: Failed to connect to the XMPP server.", hdr->name);
		return FAILED;
	}

	return SUCCESS;
}


// -------------------------------------------------------------------------------------
bool ASBox::receiveDistributeAction(TIMESTAMP t0, TIMESTAMP t1) {

	// Get XMPP messages from the broker agent (if there are).
	if (waitForXmppMessage == false) {
		return false;
	}

	string msg = this->xmppBot->receiveMessage();

	if (msg.compare("no-message") == 0) {
		// Nothing to parse.
		return true;
	}

	// Build a DistributeActionEvent object from the message
	DistributeEvent *newDistAction = DistributeEvent::create(msg);
	if (newDistAction == NULL) {
		gl_error("Failed to parse the received XMPP message: [%s].", msg.c_str());
		return false;
	} else if (this->distAction == NULL) {
		//gl_warning("%s", newDistAction->toString().c_str());
		this->distAction = newDistAction;
		return true;
	}

	//gl_warning("%s", newDistAction->toString().c_str());

	// The previous DistributeActionEvent was replaced by a new one.
	// Look for cancelled events that were active in the previous DistributeActionEvent.
	for (vector<AsdrEvent>::iterator itEvent = this->distAction->events.begin();
			itEvent != this->distAction->events.end(); ++itEvent) {

		AsdrEvent &event = *itEvent;
		if (event.eventStatus.compare("active") != 0) {
			// This event is not running, so it doesn't matter whether it was cancelled or not.
			continue;
		}

		const AsdrEvent* newEvent = DistributeEvent::findEvent(event.id, newDistAction->events);
		if ((newEvent == NULL) || (newEvent->eventStatus.compare("cancelled") == 0)) {
			// The event was active previously and now it has been cancelled by the new DistributeActionEvent.
			event.setToBeCancelled();
		}
	}

	// Replace the previous DistributeActionEvent
	delete this->distAction;
	this->distAction = newDistAction;

	return true;
}


// -------------------------------------------------------------------------------------
TIMESTAMP ASBox::presync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2 = TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


// -------------------------------------------------------------------------------------
TIMESTAMP ASBox::sync(TIMESTAMP t0, TIMESTAMP t1) {

	// Update the list of events.
	OBJECT* hdr = OBJECTHDR(this);
	if (waitForXmppMessage == true) {
		receiveDistributeAction(t0, t1);
	}

	if (this->distAction == NULL) {
		// There are not registered events. Nothing to do.
		if (defaultOadrLevel > 0) {
			commander->commit(defaultOadrLevel);
		}
		return TS_NEVER;
	}

	// Iterate over the events in order to detect:
	//  - Cancelled events.
	//  - Events to start.
	//  - Events to be finished.
	//  - Intervals.
	TIMESTAMP t2 = TS_NEVER;
	for (std::vector<AsdrEvent>::iterator itEvent = distAction->events.begin();
			itEvent != distAction->events.end(); ++itEvent) {

		AsdrEvent& event = *itEvent;
		const long endEventTime = event.start + event.duration;
		const EventState eventState = event.getExecutionState();

		if (event.isToBeCancelled()) {
			event.setExecutionState(CANCELLED);
			event.setRunningInterval(-1);
			if (eventState == STARTED) {
				commander->stop();
			}

		} else if (t1 < event.start) {
			// Nothing to do to with this event.
			t2 = min(t2, event.start);
			continue;

		} else if (t1 >= endEventTime) {
			// This event should not be running anymore.
			if (eventState == STARTED) {
				commander->stop();
				event.setExecutionState(FINISHED);
				event.setRunningInterval(-1);
			}
		} else if (eventState == NOT_STARTED) { // start <= t1 < finished
			// This event should be running.
			// Find the interval that should be executing.
			for (int nInterval = 0; nInterval < event.intervals.size(); nInterval++) {
				AsdrInterval& interval = event.intervals[nInterval];
				if ((t1 >= interval.start) && (t1 < (interval.start + interval.duration))) {
					// This interval should be running. Apply the action.
					event.setExecutionState(STARTED);
					event.setRunningInterval(nInterval);
					commander->commit(interval.payload);

					if (nInterval != (event.intervals.size() - 1)) {
						// If it is not the last interval of the event, t2 must be set to the beginning of the next event.
						t2 = min(t2, event.intervals[nInterval + 1].start);
					} else {
						// If it is the last interval, t2 must be set to the end of the event.
						t2 = min(t2, endEventTime);
						//t2 = TS_NEVER;
					}
					break;
				}
			}

		} else if (eventState == STARTED) { // start <= t1 < finished
			// Find out if t1 matchs up with either the start time of a new interval.
			for (int nInterval = 0; nInterval < event.intervals.size(); nInterval++) {
				AsdrInterval& interval = event.intervals[nInterval];
				if (t1 == interval.start) {
					event.setRunningInterval(nInterval);
					commander->commit(interval.payload);

					if (nInterval != (event.intervals.size() - 1)) {
						// If it is not the last interval of the event, t2 must be set to the beggining of the next event.
						t2 = min(t2, event.intervals[nInterval + 1].start);
					} else {
						// If it is the last interval, t2 must be set to the end of the event.
						t2 = min(t2, endEventTime);
					}
					break;

				} else if ((t1 > interval.start) && (t1 < (interval.start + interval.duration))) {
					commander->commit(interval.payload);

					if (nInterval != (event.intervals.size() - 1)) {
						// If it is not the last interval of the event, t2 must be set to the beggining of the next event.
						t2 = min(t2, event.intervals[nInterval + 1].start);
					} else {
						// If it is the last interval, t2 must be set to the end of the event.
						t2 = min(t2, endEventTime);
					}
					break;
				}
			}
		}
	}

	// TODO: Remove the finished and cancelled events.

	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


// -------------------------------------------------------------------------------------
TIMESTAMP ASBox::postsync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2= TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure
}


//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF CORE LINKAGE
//////////////////////////////////////////////////////////////////////////

EXPORT int create_ASBox(OBJECT **obj)
{
	try
	{
		*obj = gl_create_object(ASBox::oclass);
		if (*obj!=NULL)
			return OBJECTDATA(*obj,ASBox)->create();
	}
	catch (char *msg)
	{
		gl_error("create_ASBox: %s", msg);
	}
	return 0;
}

EXPORT int init_ASBox(OBJECT *obj, OBJECT *parent)
{
	try
	{
		if (obj!=NULL) {
			return OBJECTDATA(obj, ASBox)->init(parent);
		}
	} catch (char *msg)	{
		gl_error("init_ASBox(obj=%d;%s): %s", obj->id, obj->name?obj->name:"unnamed", msg);
	}
	return 0;
}

EXPORT TIMESTAMP sync_ASBox(OBJECT *obj, TIMESTAMP t1, PASSCONFIG pass)
{
	TIMESTAMP t2 = TS_NEVER;
	ASBox *my = OBJECTDATA(obj,ASBox);
	try
	{
		switch (pass) {
		case PC_PRETOPDOWN:
			t2 = my->presync(obj->clock,t1);
			break;
		case PC_BOTTOMUP:
			t2 = my->sync(obj->clock,t1);
			break;
		case PC_POSTTOPDOWN:
			t2 = my->postsync(obj->clock,t1);
			break;
		default:
			GL_THROW("invalid pass request (%d)", pass);
			break;
		}
		if (pass==clockpass)
			obj->clock = t1;
		return t2;
	}
	catch (char *msg)
	{
		gl_error("sync_ASBox(obj=%d;%s): %s", obj->id, obj->name?obj->name:"unnamed", msg);
	}
	return TS_INVALID;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------------
UserPreferences::UserPreferences() {
	this->priority = 0;
	this->levelsScheduleName = NULL;
	this->levelsScheduleDefinition = NULL;
	this->easyLevel = NO_LEVEL;
	this->hardLevel = NO_LEVEL;
	this->loadMode = NULL;
	this->levelPrice1 = NO_PRICE;
	this->levelPrice2 = NO_PRICE;
	this->levelPrice3 = NO_PRICE;
	this->startingPrice = NO_PRICE;
	return;
}


// -------------------------------------------------------------------------------------
UserPreferences::~UserPreferences() {
	return;
}

// -------------------------------------------------------------------------------------
void UserPreferences::setPriority(int32 priority) {
	this->priority = priority;
	return;
}


// -------------------------------------------------------------------------------------
int32 UserPreferences::getPriority() const {
	return this->priority;
}


// -------------------------------------------------------------------------------------
void UserPreferences::setEasyLevel(int32 easyLevel) {
	this->easyLevel = easyLevel;
	return;
}

// -------------------------------------------------------------------------------------
int32 UserPreferences::getEasyLevel() const {
	return this->easyLevel;
}

// -------------------------------------------------------------------------------------
void UserPreferences::setHardLevel(int32 hardLevel) {
	this->hardLevel = hardLevel;
	return;
}

// -------------------------------------------------------------------------------------
int32 UserPreferences::getHardLevel() const {
	return this->hardLevel;
}

// -------------------------------------------------------------------------------------
void UserPreferences::setLoadMode(char* loadMode) {
	this->loadMode = loadMode;
	return;
}

// -------------------------------------------------------------------------------------
char* UserPreferences::getLoadMode() const {
	return this->loadMode;
}

// -------------------------------------------------------------------------------------
void UserPreferences::setLevelPrice1(int32 price) {
	this->levelPrice1 = price;
}

// -------------------------------------------------------------------------------------
int32 UserPreferences::getLevelPrice1() const {
	return this->levelPrice1;
}


// -------------------------------------------------------------------------------------
void UserPreferences::setLevelPrice2(int32 price) {
	this->levelPrice2 = price;
}

// -------------------------------------------------------------------------------------
int32 UserPreferences::getLevelPrice2() const {
	return this->levelPrice2;
}


// -------------------------------------------------------------------------------------
void UserPreferences::setLevelPrice3(int32 price) {
	this->levelPrice3 = price;
}


// -------------------------------------------------------------------------------------
int32 UserPreferences::getStartingPrice() const {
	return this->startingPrice;
}


// -------------------------------------------------------------------------------------
void UserPreferences::setStartingPrice(int32 startingPrice) {
	this->startingPrice = startingPrice;
	return;
}


// -------------------------------------------------------------------------------------
int32 UserPreferences::getLevelPrice3() const {
	return this->levelPrice3;
}

// -------------------------------------------------------------------------------------
void UserPreferences::setLevelsSchedule(char* levelsScheduleName, char* levelsScheduleDefinition) {
	this->levelsScheduleName = levelsScheduleName;
	this->levelsScheduleDefinition = levelsScheduleDefinition;
	return;
}

// -------------------------------------------------------------------------------------
char* UserPreferences::getLevelsScheduleDefinition() const {
	return this->levelsScheduleDefinition;
}

// -------------------------------------------------------------------------------------
char* UserPreferences::getLevelsScheduleName() const {
	return this->levelsScheduleName;
}

// -------------------------------------------------------------------------------------
string UserPreferences::toXmlString() const {

	stringstream ss;

	ss << "<?xml version=\"1.0\" ?>" << endl;
	ss << "<userPrefs xmlns=\"http://www.siani.es/agencyservices/userprefs/1.0\">" << endl;

	if (levelsScheduleName != NULL) {
		ss << "    <levelsSchedule name=\"" << levelsScheduleName << "\"><![CDATA[" << endl;
		ss << this->levelsScheduleDefinition << endl;
		ss << "]]>" << endl;
		ss << "    </levelsSchedule>" << endl;
	}

	if (priority != -1) {
		ss << "    <priority>" << priority << "</priority>" << endl;
	}

	if (easyLevel != NO_LEVEL) {
		ss << "    <easyLevel>" << easyLevel << "</easyLevel>" << endl;
	}

	if (hardLevel != NO_LEVEL) {
		ss << "    <hardLevel>" << hardLevel << "</hardLevel>" << endl;
	}

	if (loadMode != NULL) {
		ss << "    <loadMode>" << loadMode << "</loadMode>" << endl;
	}

	if ((levelPrice1 != NO_PRICE) || (levelPrice2 != NO_PRICE) || (levelPrice3 != NO_PRICE)) {
		ss << "    <levelsPrices>" << endl;
		ss << "        <levelPrice1>" << ((levelPrice1 != NO_PRICE)? levelPrice1 : defaultPrice) << "</levelPrice1>" << endl;
		ss << "        <levelPrice2>" << ((levelPrice2 != NO_PRICE)? levelPrice2 : defaultPrice) << "</levelPrice2>" << endl;
		ss << "        <levelPrice3>" << ((levelPrice3 != NO_PRICE)? levelPrice3 : defaultPrice) << "</levelPrice3>" << endl;
		ss << "    </levelsPrices>" << endl;
	}


	if (startingPrice != NO_PRICE) {
		ss << "    <startingPrice>" << startingPrice << "</startingPrice>" << endl;
	}

	ss << "</userPrefs>" << endl;

	return ss.str();
}
