
#include "gridopservicesproxy.h"
#include <gridlabd.h>
#include <string>
#include <iostream>
#include <wonrest/wonrest.h>
#include "agencyservices.h"
#include "asutils.h"
#include "asglobals.h"


using namespace wonrest;

// ---------------------------------------------------------------------------------------
GridopRestfulProxy::~GridopRestfulProxy() {
	return;
}


// ---------------------------------------------------------------------------------------
GridopRestfulProxy::GridopRestfulProxy(string wsEndpoint, string scenarioCode) : wsEndpoint(wsEndpoint), scenarioCode(scenarioCode), idSimulation(-1)
{

	if (wsEndpoint.length() == 0) {
		gl_error("GridopRestfulProxy: the endpoint of the services layer must be defined.");
		throw;
	}

	if (scenarioCode.length() == 0) {
		gl_error("GridopRestfulProxy: A scenario code must be defined.");
		throw;
	}

	// If necessary, the 'http' prefix is added to the URL of the WS endpoint.
	if (wsEndpoint.compare(0,7, "http://") != 0) {
		wsEndpoint.insert(0,"http://");
	}

	return;
}


// ---------------------------------------------------------------------------------------
int GridopRestfulProxy::simulationStarted() {

	string strUseStartingPrice = "false";
	if (useStartingPrice > 0) {
		strUseStartingPrice = "true";
	}

	string strUseRandom = "true";
	if (useRandom == 0) {
		strUseRandom = "false";
	}

	stringstream wsPath;
	wsPath << "/simulation?scenarioCode="
			<< this->scenarioCode
			<< "&df=" << nDirectories
			<< "&of=" << overbookingFactor
			<< "&usp=" << strUseStartingPrice
			<< "&rnd=" << strUseRandom
			<< "&cf=" << constantF;

	RestClient wsClient(this->wsEndpoint.c_str());
	RestResponse wsResponse = wsClient.post(wsPath.str(), "");
	if (wsResponse.isError()) {
		gl_error("GridopRestfulProxy: simulationStarted: Error while connecting to the Grid Operator's services layer.");
		throw;
	}

	this->idSimulation = atoi(wsResponse.getMessage().c_str());

	return this->idSimulation;
}


// ---------------------------------------------------------------------------------------
void GridopRestfulProxy::simulationFinished()
{

	stringstream wsPath;
	wsPath << "/simulation/" << this->idSimulation << "/finished";
	RestClient wsClient(this->wsEndpoint.c_str());
	RestResponse wsResponse = wsClient.put(wsPath.str(), "");
	if (wsResponse.isError()) {
		gl_error("GridopRestfulProxy: simulationFinished: Error while connecting to the Grid Operator's services layer.");
		throw;
	}

	return;
}


// ---------------------------------------------------------------------------------------
GridopPause GridopRestfulProxy::simulationPaused(TIMESTAMP modelTimestamp, TIMESTAMP realTimestamp)
{
	string strModelTime = AsUtils::formatTimestamp(modelTimestamp);
	string strSystemTime = AsUtils::formatTimestamp(realTimestamp);

	stringstream wsPath;
	wsPath << "/simulation/" << this->idSimulation << "/paused?mt=" << strModelTime << "&rt=" << strSystemTime;

	RestClient wsClient(this->wsEndpoint.c_str());
	RestResponse wsResponse = wsClient.put(wsPath.str(), "");
	if (wsResponse.isError()) {
		gl_error("GridopRestfulProxy: simulationPaused: Error while connecting to the Grid Operator's services layer.");
		throw;
	}

	vector<string> tokens;
	AsUtils::split(wsResponse.getMessage(), ',', tokens);
	GridopPause p;
	long nextPause = atol(tokens[0].c_str());
	p.nextPause = (nextPause == -1)? TS_NEVER : (nextPause / 1000);
	p.eventTriggered = (tokens[1].compare("true") == 0) || (tokens[1].compare("1") == 0);

	return p;
}


// ---------------------------------------------------------------------------------------
void GridopRestfulProxy::createASPEM(string name, string longName, string host, int port) {

	stringstream ss;
	ss << "<?xml version=\"1.0\" standalone=\"yes\"?>" << endl;
	ss << "<aspem>" << endl;
	ss << "    <code>" << name << "</code>" << endl;
	ss << "    <name>" << longName << "</name>" << endl;
	ss << "    <host>" << host << "</host>" << endl;
	ss << "    <port>" << port << "</port>" << endl;
	ss << "</aspem>";

	stringstream wsPath;
	wsPath << "/simulation/" << this->idSimulation << "/aspem";

	RestClient wsClient(this->wsEndpoint.c_str());
	wsClient.addHeader("Content-Type", "application/xml");
	RestResponse wsResponse = wsClient.post(wsPath.str(), ss.str());
	if (wsResponse.isError()) {
		gl_error("AspemRestfulProxy: createASPEM: Error while connecting to the ASPEM's services layer.");
		throw;
	}

	return;
}


// ---------------------------------------------------------------------------------------
GridopServicesProxy* GridopServicesProxyFactory::getProxy(string wsEndpoint, string scenarioCode)
{
	GLOBALVAR *useWsMock = gl_global_find("useWsMock");

	if (useWsMock == NULL) {
		return new GridopRestfulProxy(wsEndpoint, scenarioCode);
	}

	return new GridopMockProxy();
}
