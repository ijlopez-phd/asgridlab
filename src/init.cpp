#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <wonrest/wonrest.h>
#include "gridlabd.h"
#include "schedule.h"
#include "agencyservices.h"
#include "asbox.h"
#include "aspem.h"
#include "gridop.h"
#include "distributeevent.h"
#include "asglobals.h"

using namespace wonrest;

EXPORT CLASS *init(CALLBACKS *fntable, MODULE *module, int argc, char *argv[])
{
	if (set_callback(fntable)==NULL) {
		errno = EINVAL;
		return NULL;
	}

	gl_global_create("agencyservices::nodeName",
			PT_char1024, &nodeName,
			PT_ACCESS, PA_PUBLIC,
			PT_DESCRIPTION, "Name of the node that is being simulated.",
			NULL);

	gl_global_create("agencyservices::scenarioCode",
			PT_char256, &scenarioCode,
			PT_ACCESS, PA_PUBLIC,
			PT_DESCRIPTION, "Code of the scenario that is being simulated.",
			NULL);

    gl_global_create("agencyservices::maxStepTime",
                    PT_int32, &maxStepTime,
                    PT_ACCESS, PA_PUBLIC,
                    PT_DESCRIPTION, "Maximum simulation time (secs) without calling the Grid Operator.",
                    NULL);

	gl_global_create("agencyservices::xmppUrl",
			PT_char1024, &xmppUrl,
			PT_ACCESS, PA_PUBLIC,
			PT_DESCRIPTION, "Url to connect with the XMPP server.",
			NULL);

	gl_global_create("agencyservices::oadrLevel",
			PT_int32, &defaultOadrLevel,
			PT_ACCESS, PA_PUBLIC,
			PT_DESCRIPTION, "Level of the global DR signal to be applied by default.",
			NULL);

//	gl_global_create("agencyservices::nDirectories",
//			PT_int32, &nDirectories,
//			PT_ACCESS, PA_PUBLIC,
//			PT_DESCRIPTION, "Number of Directory Facilitators to use in the simulation.",
//			NULL);
//
//	gl_global_create("agencyservices::overbookingFactor",
//			PT_int32, &overbookingFactor,
//			PT_ACCESS, PA_PUBLIC,
//			PT_DESCRIPTION, "Overbooking factor to use in the simulation.",
//			NULL);


#ifdef OPTIONAL
	/* TODO: publish global variables (see class_define_map() for details) */
	//gl_global_create(char *name, ..., NULL);
	/* TODO: use gl_global_setvar, gl_global_getvar, and gl_global_find for access */
#endif


	// Registering classes of the module
	new ASBox(module);
	new ASPEM(module);
	new GridOperator(module);

	/* always return the first class registered */
	return ASBox::oclass;
}


CDECL int do_kill()
{
	// Tell everybody that the simulation is over.

	// - Tell all ASBox objects that the simulation is over.
	FINDLIST *asboxList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "ASBox", FT_END);
	for (int n = 0; n < asboxList->hit_count; n++) {
		OBJECT *objASBox = gl_find_next(asboxList, NULL);
		ASBox *asbox = OBJECTDATA(objASBox, ASBox);
		asbox->finish();
	}

	// - Tell all Aspem objects that the simulation is over.
	FINDLIST *aspemList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "ASPEM", FT_END);
	for (int n = 0; n < aspemList->hit_count; n++) {
		OBJECT *objAspem = gl_find_next(aspemList, NULL);
		ASPEM *aspem = OBJECTDATA(objAspem, ASPEM);
		aspem->finish();
	}

	// - Tell the GridOperator that the simulation is over.
    FINDLIST *gridOperatorList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "GridOperator", FT_END);
    if (gridOperatorList->hit_count > 0) {
            OBJECT *objGridOperator = gl_find_next(gridOperatorList, NULL);
            GridOperator *gridop = OBJECTDATA(objGridOperator, GridOperator);
            gridop->finish();
    }

	// Clear other resources
	DistributeEvent::clearCache();


	/* if global memory needs to be released, this is a good time to do it */
	return 0;
}

EXPORT int check() {
	return 0;
}

EXPORT int import_file(char *filename) {
	return 0;
}

EXPORT int export_file(char *filename) {
	return 0;
}

EXPORT int kmldump(FILE *fp, OBJECT *obj) {
	return 0;
}
