
#ifndef _AGENCY_SERVICES_H
#define _AGENCY_SERVICES_H

#define NULL_SIMULATION_ID -1

/**
 * by placing this include early (vector), the error due to the redifinition of the 'min' and 'max'
 * macros is avoided in most cases.
 */
#include <vector>

#include <stdarg.h>
#include "asglobals.h"
#include "gridlabd.h"

/* optional exports */
#ifdef OPTIONAL
/* TODO: define this function to enable checks routine */
EXPORT int check(void);

/* TODO: define this function to allow direct import of models */
EXPORT int import_file(char *filename);

/* TODO: define this function to allow direct export of models */
EXPORT int export_file(char *filename);

/* TODO: define this function to allow export of KML data for a single object */
EXPORT int kmldump(FILE *fp, OBJECT *obj);
#endif

#endif /* _AGENCY_SERVICES_H */
