

#ifndef _DRMODEL_H_
#define _DRMODEL_H_

#include <string>
#include <vector>

/**
 * Forward declarations.
 */
class DistributeEvent;
class AsdrTargets;
class AsdrEvent;
class AsdrInterval;



/**
 * Data used by the XML parser to pass information between calls.
 */
typedef struct {
	std::string parentTag;
	std::string tagBreadCrumbs;
	DistributeEvent* distributeEvent;
} AsXmlParserData;



/**
 * DistributeEvent.
 * Wrapper the essential data of the element 'DistributeEventAction' corresponding to the
 * protocol OpenADR 2.0.
 *
 * It includes a small cache intended to avoid parsing the XML of a DistributeEventAction that
 * has been parsed recently.
 *
 */
class DistributeEvent {

public:

	/** Write into a string information about the content of the instance. */
	std::string const toString() const;

	/** Create an instance of DistributeEvent from a XML. */
	static DistributeEvent* create(std::string const& xml);

	/** Clear the cache. */
	static void clearCache();

	/** Look for an event into a vector of them. */
	static const AsdrEvent* findEvent(std::string idEvent, std::vector<AsdrEvent> const &events);


private:

	/** Constructor. */
	DistributeEvent();

	/** Copy constructor. */
	DistributeEvent(DistributeEvent const &other);

public:

	int requestId;
	unsigned int hash;
	std::vector<AsdrEvent> events;

private:

	static DistributeEvent *cachedDistributeEvent;
};



/**
 * AsdrTargets.
 * Lists of the targets of an Oadr event.
 */
class AsdrTargets {

public:

	/** Write into a string information about the content of the instance. */
	std::string const toString() const;

public:
	std::vector<std::string> groups;
	std::vector<std::string> resources;
	std::vector<std::string> vendors;
	std::vector<std::string> parties;
};


/**
 * States that can get an Event during its life-cycle.
 */
enum EventState {NOT_STARTED, STARTED, FINISHED, CANCELLED};


/**
 * AsdrEvent.
 * Simplification of the element 'oadrEvent' contained in a DistributeEventAction.
 *
 */
class AsdrEvent {

public:

	/** Constructor. */
	AsdrEvent();

	/** Operator = */
	AsdrEvent& operator=(AsdrEvent const &oEvent);

	/** Write into a string information about the content of the instance. */
	std::string const toString() const;

	/** Set this event to be cancelled the next time it is processed. */
	void setToBeCancelled();

	/** Return whether the should be cancelled or not. */
	bool isToBeCancelled() const;

	/** Set the index of the interval that is currently running. */
	void setRunningInterval(int idx);

	/** Get the index of the interval that is currently running. */
	int getRunningInterval() const;

	/** Set the execution state of the event. */
	void setExecutionState(EventState state);

	/** Get the execution state of the event. */
	EventState getExecutionState() const;

public:
	std::string id; /** Identifier of the event. */
	std::string eventStatus; /** none, far, near, active, completed, cancelled. */
	int modificationNumber; /** Version of the event. */
	int priority; /** Priority of the event. Lower the number higher the priority. Zero indicates no priority. */
	long start; /** Date at which the event starts. */
	long duration; /** Duration of the event in seconds. */
	long notifDuration; /** Period of the notification. */
	std::string signalType; /** delta, level, multiplier, price, priceMultiplier, priceRelative, setpoint, product. */
	std::vector<AsdrInterval> intervals; /** Intervals of the compose the event. */
	AsdrTargets targets;

private:

	bool toBeCancelled;
	int idxRunningInterval;
	EventState state;

};



/**
 * AsdrInterval.
 * Represent an interval of a AsdrEvent.
 * The value (payload) of the signal is constant along the interval.
 */
class AsdrInterval {

public:

	/** Copy constructor. */
	AsdrInterval(AsdrInterval const& other);

	/** Constructor. */
	AsdrInterval();

	/** Constructor. */
	AsdrInterval(long start, long duration, float payload);

	/** Write into a string information about the content of the instance. */
	std::string const toString() const;

public:

	long start; /** Date at which the interval starts. */
	long duration; /** Duration (in millisecons) of the interval. */
	float payload; /** Value to be applied during the interval. */
};


#endif /* _DRMODEL_H_ */
