#ifndef _ASPEM_H_
#define _ASPEM_H_

#include "agencyservices.h"
#include "gridlabd.h"
#include "aspemservicesproxy.h"

class ASPEM {

public:

	// GridLAB-D standard functions
	ASPEM(MODULE *module);
	~ASPEM();
	int init(OBJECT *parent);
	int create();
	TIMESTAMP presync(TIMESTAMP t0, TIMESTAMP t1);
	TIMESTAMP sync(TIMESTAMP t0, TIMESTAMP t1);
	TIMESTAMP postsync(TIMESTAMP t0, TIMESTAMP t1);

	const char* getLongName() const;
	AspemServicesProxy* getServicesProxy() const;
	void finish();

public:

	static CLASS *oclass;
	static ASPEM *defaults;

private:
	char1024 wsEndpoint;
	char1024 longName;
	AspemServicesProxy *wsProxy;
};

typedef struct {
	string host;
	int port;
} AspemPath;


#endif /* _ASPEM_H_ */
