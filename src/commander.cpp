#include <iostream>
#include <gridlabd.h>
#include <find.h>
#include "commander.h"


/**
 * Class AsdrCommander.
 */

// -------------------------------------------------------------------------------------
AsdrCommander::~AsdrCommander() {
	return;
}


// -------------------------------------------------------------------------------------
AsdrCommander::AsdrCommander() {
	return;
}


/**
 * Commander 02.
 */
// -------------------------------------------------------------------------------------
AsdrCommander02::~AsdrCommander02() {
	if (this->zipLoads != NULL) {
		free(this->zipLoads);
	}

	return;
}

// -------------------------------------------------------------------------------------
AsdrCommander02::AsdrCommander02(house_e* house) : AsdrCommander(), waterHeater(NULL), house(house), nZipLoads(0), zipLoads(NULL) {
	if (house == NULL) {
		// Commander must be initiated with a valid instance of house_e.
		throw -1;
	}

	OBJECT *hdr = OBJECTHDR(house);

	// Look for the loads of the house that are affected by the DR signals.

	//  a.- Look for the waterheater.
	FINDLIST *waterheatersList = gl_find_objects(
			FL_NEW,
			FT_CLASS, SAME, "waterheater",
			AND,
			FT_PARENT, FT_ID, EQ, hdr->id,
			FT_END);
	if (waterheatersList->hit_count > 0) {
		this->waterHeater = OBJECTDATA(gl_find_next(waterheatersList, NULL), waterheater);
		if (waterheatersList->hit_count > 1) {
			gl_warning("AsdrCommander02: constructor: House '%s' has more than one waterheater.", hdr->name);
		}
	} else {
		this->waterHeater = NULL;
	}

	//  b.- Look for the ZIPloads.
	FINDLIST *zipLoadsList = gl_find_objects(
			FL_NEW,
			FT_CLASS, SAME, "ZIPload",
			AND,
			FT_PARENT, FT_ID, EQ, hdr->id,
			FT_END);
	if (zipLoadsList->hit_count > 0) {
		this->nZipLoads = zipLoadsList->hit_count;
		this->zipLoads = (OBJECT**)malloc(sizeof(OBJECT*) * (this->nZipLoads + 1));
		OBJECT *objZip = NULL;
		for (int n = 0; n < this->nZipLoads; n++) {
			objZip = gl_find_next(zipLoadsList, objZip);
			this->zipLoads[n] = objZip;
		}
		// An extra NULL is added in order to mark the end of the array.
		this->zipLoads[zipLoadsList->hit_count] = NULL;

	} else {
		this->zipLoads = NULL;
		this->nZipLoads = 0;
	}

	// Save the initial values.
	this->hvacDefaultStatus.coolingSetpoint = house->cooling_setpoint;
	this->hvacDefaultStatus.heatingSetpoint = house->heating_setpoint;
	this->defaultDeadband = house->thermostat_deadband;

	return;
}

// -------------------------------------------------------------------------------------
void AsdrCommander02::commit(float payload) {

	int ipayload = (int)payload;
	if (ipayload == LEVEL_NORMAL) { // normal
		this->stop();

	} else if (ipayload == LEVEL_MODERATE) { // moderate

		// Turn off the HVAC.
		house->re_override = OV_OFF;
		house->thermostat_deadband = 9999;


	} else if (ipayload == LEVEL_HIGH) { // high

		// Turn off the HVAC.
		house->re_override = OV_OFF;
		house->thermostat_deadband = 9999;

		// Turn off the water heater.
		if (waterHeater != NULL) {
			waterHeater->re_override = OV_OFF;
		}


	} else if (ipayload == LEVEL_CRITICAL) { // critical

		// Turn off the water heater.
		if (waterHeater != NULL) {
			waterHeater->re_override = OV_OFF;
		}

		// Turn off the HVAC.
		house->re_override = OV_OFF;
		house->thermostat_deadband = 9999;

		// Turn off the ZIP loads.
		if (zipLoads != NULL) {
			int n = 0;
			while (zipLoads[n] != NULL) {
				ZIPload* load = OBJECTDATA(zipLoads[n], ZIPload);
				load->pCircuit->status = BRK_OPEN;
				n++;
			}
		}
	}

	return;
}

// -------------------------------------------------------------------------------------
void AsdrCommander02::stop() {

	// Restore the values of the HVAC.
	house->heating_setpoint = hvacDefaultStatus.heatingSetpoint;
	house->cooling_setpoint = hvacDefaultStatus.coolingSetpoint;
	house->re_override = OV_NORMAL;
	house->thermostat_deadband = defaultDeadband;

	// Restore the values of the water heater.
	if (waterHeater != NULL) {
		waterHeater->re_override = OV_NORMAL;
	}

	// Turn on the ZIP loads.
	if (zipLoads != NULL) {
		int n = 0;
		while (zipLoads[n] != NULL) {
			ZIPload* load = OBJECTDATA(zipLoads[n], ZIPload);
			load->pCircuit->status = BRK_CLOSED;
			n++;
		}
	}

	return;
}


/**
 *
 * Class BasicAsdrCommander.
 *
 */

// -------------------------------------------------------------------------------------
BasicAsdrCommander::~BasicAsdrCommander() {

	if (this->zipLoads != NULL) {
		free(this->zipLoads);
	}

	return;
}


// -------------------------------------------------------------------------------------
BasicAsdrCommander::BasicAsdrCommander(house_e* house) : AsdrCommander(), house(house), waterHeater(NULL), nZipLoads(0), zipLoads(NULL) {

	if (house == NULL) {
		// Commander must be initiated with a valid instance of house_e.
		throw -1;
	}

	OBJECT *hdr = OBJECTHDR(house);

	// Look for the loads of the house that are affected by the DR signals.

	//  a.- Look for the waterheater.
	FINDLIST *waterheatersList = gl_find_objects(
			FL_NEW,
			FT_CLASS, SAME, "waterheater",
			AND,
			FT_PARENT, FT_ID, EQ, hdr->id,
			FT_END);
	if (waterheatersList->hit_count > 0) {
		this->waterHeater = OBJECTDATA(gl_find_next(waterheatersList, NULL), waterheater);
		this->waterHeaterDefaultStatus.setpoint = this->waterHeater->tank_setpoint;
		if (waterheatersList->hit_count > 1) {
			gl_warning("BasicAsdrCommander: constructor: House '%s' has more than one waterheater.", hdr->name);
		}


	} else {
		this->waterHeater = NULL;
	}

	//  b.- Look for the ZIPloads.
	FINDLIST *zipLoadsList = gl_find_objects(
			FL_NEW,
			FT_CLASS, SAME, "ZIPload",
			AND,
			FT_PARENT, FT_ID, EQ, hdr->id,
			FT_END);
	if (zipLoadsList->hit_count > 0) {
		this->nZipLoads = zipLoadsList->hit_count;
		this->zipLoads = (OBJECT**)malloc(sizeof(OBJECT*) * (this->nZipLoads + 1));
		OBJECT *objZip = NULL;
		for (int n = 0; n < this->nZipLoads; n++) {
			objZip = gl_find_next(zipLoadsList, objZip);
			this->zipLoads[n] = objZip;
		}
		// An extra NULL is added in order to mark the end of the array.
		this->zipLoads[zipLoadsList->hit_count] = NULL;

	} else {
		this->zipLoads = NULL;
		this->nZipLoads = 0;
	}

	// Save the initial status of both the water heater and the HVAC.
	if (this->waterHeater != NULL) {
		this->waterHeaterDefaultStatus.setpoint = this->waterHeater->tank_setpoint;
	} else {
		this->waterHeaterDefaultStatus.setpoint = -1;
	}

	this->hvacDefaultStatus.coolingSetpoint = house->cooling_setpoint;
	this->hvacDefaultStatus.heatingSetpoint = house->heating_setpoint;

	return;
}


// -------------------------------------------------------------------------------------
void BasicAsdrCommander::stop() {

	// Restore the values of the HVAC.
	house->heating_setpoint = hvacDefaultStatus.heatingSetpoint;
	house->cooling_setpoint = hvacDefaultStatus.coolingSetpoint;

	// Restore the values of the water heater.
	if (waterHeater != NULL) {
		waterHeater->re_override = OV_NORMAL;
		waterHeater->tank_setpoint = waterHeaterDefaultStatus.setpoint;
	}

	// Turn on the ZIP loads.
	if (zipLoads != NULL) {
		int n = 0;
		while (zipLoads[n] != NULL) {
			ZIPload* load = OBJECTDATA(zipLoads[n], ZIPload);
			load->pCircuit->status = BRK_CLOSED;
			n++;
		}
	}

	return;
}


// -------------------------------------------------------------------------------------
void BasicAsdrCommander::commit(float payload) {

	int ipayload = (int)payload;
	if (ipayload == LEVEL_NORMAL) { // normal
		this->stop();

	}else if (ipayload == LEVEL_MODERATE) { // moderate
		if (waterHeater != NULL) {
			waterHeater->re_override = OV_NORMAL;
			waterHeater->tank_setpoint = waterHeaterDefaultStatus.setpoint - DELTA_WATER_HEATER_MODERATE;
		}

		if (house->heating_setpoint > HEATER_SETPOINT_MODERATE) {
			house->heating_setpoint = HEATER_SETPOINT_MODERATE;
		}

		if (house->cooling_setpoint < COOLER_SETPOINT_MODERATE) {
			house->cooling_setpoint = COOLER_SETPOINT_MODERATE;
		}

	} else if (ipayload == LEVEL_HIGH) { // high

		if (waterHeater != NULL) {
			waterHeater->re_override = OV_NORMAL;
			waterHeater->tank_setpoint = waterHeaterDefaultStatus.setpoint - DELTA_WATER_HEATER_HIGH;
		}

		if (house->heating_setpoint > HEATER_SETPOINT_HIGH) {
			house->heating_setpoint = HEATER_SETPOINT_HIGH;
		}

		if (house->cooling_setpoint < COOLER_SETPOINT_HIGH) {
			house->cooling_setpoint = COOLER_SETPOINT_HIGH;
		}


	} else if (ipayload == LEVEL_CRITICAL) { // critical

		// Turn off the water heater.
		if (waterHeater != NULL) {
			waterHeater->re_override = OV_OFF;
		}

		// Turn off the HVAC.
		house->re_override = OV_OFF;
		house->thermostat_deadband = 9999;

		// Turn off the ZIP loads.
		if (zipLoads != NULL) {
			int n = 0;
			while (zipLoads[n] != NULL) {
				ZIPload* load = OBJECTDATA(zipLoads[n], ZIPload);
				load->pCircuit->status = BRK_OPEN;
				n++;
			}
		}
	}

	return;
}


/**
 *
 * Class AsdrCommanderFactory.
 *
 */
AsdrCommander* AsdrCommanderFactory::getCommander(house_e* house) {
	GLOBALVAR *useCommanderMock = gl_global_find("useCommanderMock");

	if (useCommanderMock == NULL) {
		//return new BasicAsdrCommander(house);
		return new AsdrCommander02(house);
	}

	return new  MockAsdrCommander();
}
