

#include "aspemservicesproxy.h"
#include <gridlabd.h>
#include <string>
#include <iostream>
#include <wonrest/wonrest.h>
#include "agencyservices.h"
#include "asutils.h"

using namespace wonrest;

// -------------------------------------------------------------------------------------
AspemRestfulProxy::~AspemRestfulProxy() {
	return;
}


// -------------------------------------------------------------------------------------
AspemRestfulProxy::AspemRestfulProxy(string wsEndpoint, int idSimulation) : wsEndpoint(wsEndpoint), idSimulation(idSimulation) {

	if (this->wsEndpoint.length() == 0) {
		gl_error("AspemRestfulProxy: the endpoint of the services layer must be defined.");
		throw;
	}


	// If necessary, the 'http' prefix is added to the URL of the WS endpoint.
	if (this->wsEndpoint.compare(0,7, "http://") != 0) {
		this->wsEndpoint.insert(0,"http://");
	}

	return;
}


// -------------------------------------------------------------------------------------
void AspemRestfulProxy::simulationStarted() {

	stringstream wsPath;
	wsPath << "/simulation/" << this->idSimulation << "/start";

	RestClient wsClient(this->wsEndpoint.c_str());
	RestResponse wsResponse = wsClient.post(wsPath.str(), "");
	if (wsResponse.isError()) {
		gl_error("AspemRestfulProxy: simulationStarted: Error while connecting with the ASPEM's services layer.");
		throw;
	}

	return;
}


// -------------------------------------------------------------------------------------
void AspemRestfulProxy::simulationFinished() {
	stringstream wsPath;
	wsPath << "/simulation/" << this->idSimulation << "/finished";
	RestClient wsClient(this->wsEndpoint.c_str());
	RestResponse wsResponse = wsClient.put(wsPath.str(), "");
	if (wsResponse.isError()) {
		gl_error("AspemRestfulProxy: simulationFinished: Error while connecting with the ASPEM's services layer.");
		throw;
	}

	return;
}


// -------------------------------------------------------------------------------------
void AspemRestfulProxy::addAsBox(string code, UserPreferences *userPrefs) {
	stringstream wsPath;
	wsPath << "/simulation/" << idSimulation << "/asbox?code=" << code;
	RestClient wsClient(this->wsEndpoint.c_str());
	wsClient.addHeader("Content-Type", "application/xml");
	RestResponse wsResponse = wsClient.post(wsPath.str(), userPrefs->toXmlString());
	if (wsResponse.isConnectionError()) {
		gl_error("AspemRestfulProxy: addAsBox: Error while connecting with the ASPEM's services layer.");
		throw;
	}

	return;
}


// -------------------------------------------------------------------------------------
AspemServicesProxy* AspemServicesProxyFactory::getProxy(string wsEndpoint, int idSimulation)
{
	GLOBALVAR *useWsMock = gl_global_find("useWsMock");

	if (useWsMock == NULL) {
		return new AspemRestfulProxy(wsEndpoint, idSimulation);
	}

	return new AspemMockProxy();
}
