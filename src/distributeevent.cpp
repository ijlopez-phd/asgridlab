
#include "distributeevent.h"
#include <string.h>
#include <time.h>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <expat.h>
#include <gridlabd.h>


/**
 * Forward declaration of static functions (not methods).
 */
void XMLCALL startElement(void *data, const char *el, const char **attr);
void XMLCALL characterData(void *data, const char *s, int len);
void XMLCALL endElement(void *data, const char *el);

static int toInt(const char *s, int len);
//static TIMESTAMP xmlDateToTimestamp(std::string const &);
static TIMESTAMP xmlDateToTimestampGridlab(std::string const &);
static std::vector<std::string>& splitTokens(std::string const &s, char delim, std::vector<std::string> &elems);
static long durationToSeconds(std::string const &);
static unsigned int hash_str(const char *);


/**
 * Static initializator.
 */
DistributeEvent* DistributeEvent::cachedDistributeEvent = NULL;




/**
 * Class DistributeEvent.
 */

// ---------------------------------------------------------------------------------------
DistributeEvent* DistributeEvent::create(std::string const& xml) {

	// If the cache contains the result of parsing the XML, it is just necessary
	// to return a copy of the cached element.
	unsigned int xmlHash = hash_str(xml.c_str());
	if ((cachedDistributeEvent != NULL) && (cachedDistributeEvent->hash == xmlHash)) {
		return new DistributeEvent(*cachedDistributeEvent);
	}

	// The XML is not cached, so it is necessary to parse it and build a new
	// DistributeActionEvent.
	AsXmlParserData parserData = {"", "", new DistributeEvent};
	XML_Parser parser = XML_ParserCreate(NULL);
	XML_SetUserData(parser, &parserData);
	XML_SetElementHandler(parser, startElement, endElement);
	XML_SetCharacterDataHandler(parser, characterData);
	XML_Status status = XML_Parse(parser, xml.c_str(), xml.size(), 1);
	XML_ParserFree(parser);

	if (status == XML_STATUS_ERROR) {
		return NULL;
	}

	parserData.distributeEvent->hash = xmlHash;
	if (cachedDistributeEvent != NULL) {
		delete cachedDistributeEvent;
	}
	cachedDistributeEvent = parserData.distributeEvent;

	return new DistributeEvent(*cachedDistributeEvent);
}


// ---------------------------------------------------------------------------------------
void DistributeEvent::clearCache() {
	if (cachedDistributeEvent == NULL) {
		return;
	}

	delete cachedDistributeEvent;
	cachedDistributeEvent = NULL;

	return;
}


// ---------------------------------------------------------------------------------------
const AsdrEvent* DistributeEvent::findEvent(std::string idEvent, std::vector<AsdrEvent> const &events) {

	for (std::vector<AsdrEvent>::const_iterator it = events.begin(); it != events.end(); ++it) {
		if ((*it).id.compare(idEvent) == 0) {
			return &(*it);
		}
	}

	return NULL;
}


// ---------------------------------------------------------------------------------------
DistributeEvent::DistributeEvent() : requestId(0), hash(0) {
	return;
}


// ---------------------------------------------------------------------------------------
DistributeEvent::DistributeEvent(DistributeEvent const &other) {

	this->requestId = other.requestId;
	this->hash = other.hash;

	for (std::vector<AsdrEvent>::const_iterator itEvent = other.events.begin();
			itEvent != other.events.end(); ++itEvent) {

		AsdrEvent thisEvent = *itEvent;
		this->events.push_back(thisEvent);
	}

	return;
}


// ---------------------------------------------------------------------------------------
std::string const DistributeEvent::toString() const {
	std::stringstream ss;

	ss << std::endl << "id : " << this->requestId << std::endl;
	for (std::vector<AsdrEvent>::const_iterator itEvent = events.begin(); itEvent != events.end(); ++itEvent) {
		ss << "-------------------------" << std::endl;
		ss << (*itEvent).toString();
		ss << "-------------------------" << std::endl;
	}

	return ss.str();
}




/**
 * Class AsdrEvent.
 */

// ---------------------------------------------------------------------------------------
AsdrEvent::AsdrEvent() : toBeCancelled(false), idxRunningInterval(-1), state(NOT_STARTED) {
	this->eventStatus = "none";
	this->modificationNumber = 0;
	this->priority = 0;
	this->start = -1;
	this->duration = -1;
	this->notifDuration = 0;

	return;
}


// ---------------------------------------------------------------------------------------
AsdrEvent& AsdrEvent::operator =(const AsdrEvent &oEvent) {

	this->id = oEvent.id;
	this->eventStatus = oEvent.eventStatus;
	this->modificationNumber = oEvent.modificationNumber;
	this->priority = oEvent.priority;
	this->start = oEvent.start;
	this->duration = oEvent.duration;
	this->notifDuration = oEvent.notifDuration;
	this->signalType = oEvent.signalType;
	this->targets = oEvent.targets;
	this->toBeCancelled = oEvent.toBeCancelled;
	this->idxRunningInterval = oEvent.idxRunningInterval;
	this->state = oEvent.state;

	for (std::vector<AsdrInterval>::const_iterator itInterval = oEvent.intervals.begin();
			itInterval != oEvent.intervals.end(); ++itInterval) {
		AsdrInterval thisInterval(*itInterval);
		this->intervals.push_back(thisInterval);
	}

	return *this;
}


// ---------------------------------------------------------------------------------------
void AsdrEvent::setToBeCancelled() {
	this->toBeCancelled = true;
	return;
}


// ---------------------------------------------------------------------------------------
bool AsdrEvent::isToBeCancelled() const {
	return this->toBeCancelled;
}


// ---------------------------------------------------------------------------------------
void AsdrEvent::setRunningInterval(int idx) {
	this->idxRunningInterval = idx;
	return;
}


// ---------------------------------------------------------------------------------------
int AsdrEvent::getRunningInterval() const {
	return this->idxRunningInterval;
}


// ---------------------------------------------------------------------------------------
void AsdrEvent::setExecutionState(EventState state) {
	this->state = state;
	return;
}


// ---------------------------------------------------------------------------------------
EventState AsdrEvent::getExecutionState() const {
	return this->state;
}

// ---------------------------------------------------------------------------------------
std::string const AsdrEvent::toString() const {
	std::stringstream ss;
	ss << "    - id : " << this->id << std::endl;
	ss << "    - status : " << this->eventStatus << std::endl;
	ss << "    - priority : " << this->priority << std::endl;
	ss << "    - start : " << this->start << std::endl;
	ss << "    - duration : " << this->duration << std::endl;
	ss << "    - notifDuration : " << this->notifDuration << std::endl;
	ss << "    - signalType : " << this->signalType << std::endl;

	for (std::vector<AsdrInterval>::const_iterator itInterval = intervals.begin(); itInterval != intervals.end(); ++itInterval) {
		ss << (*itInterval).toString();
	}

	ss << targets.toString();

	return ss.str();
}



/**
 * Class AsdrInterval.
 */

// ---------------------------------------------------------------------------------------
AsdrInterval::AsdrInterval() : start(0), duration(0), payload(0) {
	return;
}


// ---------------------------------------------------------------------------------------
AsdrInterval::AsdrInterval(AsdrInterval const& other) {
	this->start = other.start;
	this->duration = other.duration;
	this->payload = other.payload;
	return;
}

// ---------------------------------------------------------------------------------------
AsdrInterval::AsdrInterval(long start, long duration, float payload) : start(start), duration(duration), payload(payload) {
	return;
}

// ---------------------------------------------------------------------------------------
std::string const AsdrInterval::toString() const {
	std::stringstream ss;
	ss << "        - start : " << this->start << std::endl;
	ss << "        - duration : " << this->duration << std::endl;
	ss << "        - payload : " << this->payload << std::endl;

	return ss.str();
}




/**
 * Class AsdrTargets.
 */
// ---------------------------------------------------------------------------------------
std::string const AsdrTargets::toString() const {
	std::stringstream ss;
	ss << "Groups : ";
	for (std::vector<std::string>::const_iterator itGroup = groups.begin(); itGroup != groups.end(); itGroup++) {
		ss << "  " << *itGroup;
	}
	ss << std::endl;

	ss << "Resources : ";
	for (std::vector<std::string>::const_iterator itRes = resources.begin(); itRes != resources.end(); itRes++) {
		ss << "  " << *itRes;
	}
	ss << std::endl;

	ss << "Vendors : ";
	for (std::vector<std::string>::const_iterator itVen = vendors.begin(); itVen != vendors.end(); itVen++) {
		ss << "  " << *itVen;
	}
	ss << std::endl;

	ss << "Parties : ";
	for (std::vector<std::string>::const_iterator itParties = parties.begin(); itParties != parties.end(); itParties++) {
		ss << "  " << *itParties;
	}
	ss << std::endl;

	return ss.str();
}




/**
 * XML Parsing.
 */

// ---------------------------------------------------------------------------------------
void XMLCALL startElement(void *data, const char *el, const char **attr) {

	AsXmlParserData *userData = (AsXmlParserData*)data;

	// Remove the prefix from the tag name.
	std::string tagName(el);
	size_t prefixPos = tagName.find_first_of(':');
	if (prefixPos != std::string::npos) {
		tagName.erase(0, prefixPos + 1);
	}

	userData->parentTag = tagName;
	userData->tagBreadCrumbs += " " + tagName;

	if (tagName.compare("oadrEvent") == 0) {
		userData->distributeEvent->events.push_back(AsdrEvent());

	} else if (tagName.compare("interval") == 0) {
		AsdrInterval interval;
		AsdrEvent& event = userData->distributeEvent->events.back();
		if (event.intervals.size() == 0) {
			// This is the first interval. Its start time is equal to the start time of the event.
			interval.start = event.start;
		} else {
			AsdrInterval& lastInterval = event.intervals.back();
			interval.start = lastInterval.start + lastInterval.duration;
		}
		event.intervals.push_back(interval);
	}

	return;
}


// ---------------------------------------------------------------------------------------
void XMLCALL characterData(void *data, const char *s, int len) {

	AsXmlParserData *userData = (AsXmlParserData*)data;

	std::string &parent = userData->parentTag;
	if (parent.compare("requestID") == 0) {
		userData->distributeEvent->requestId = toInt(s, len);

	} else if (parent.compare("eventID") == 0) {
		userData->distributeEvent->events.back().id = std::string(s, len);

	} else if (parent.compare("modificationNumber") == 0) {
		userData->distributeEvent->events.back().modificationNumber = toInt(s, len);

	} else if (parent.compare("priority") == 0) {
		userData->distributeEvent->events.back().priority = toInt(s, len);

	} else if (parent.compare("eventStatus") == 0) {
		std::string status(s, len);
		if (status.compare("none") == 0) {
			status = "far";
		}
		userData->distributeEvent->events.back().eventStatus = status;

	} else if (parent.compare("date-time") == 0) {
		if (userData->tagBreadCrumbs.find("eiActivePeriod properties dtstart") != std::string::npos) {
			userData->distributeEvent->events.back().start = xmlDateToTimestampGridlab(std::string(s, len));
		}

	} else if (parent.compare("duration") == 0) {
		if (userData->tagBreadCrumbs.find("eiActivePeriod properties duration") != std::string::npos) {
			userData->distributeEvent->events.back().duration = durationToSeconds(std::string(s, len));

		} else if (userData->tagBreadCrumbs.find("eiActivePeriod properties x-eiNotification") != std::string::npos) {
			userData->distributeEvent->events.back().notifDuration = durationToSeconds(std::string(s, len));

		} else if (userData->tagBreadCrumbs.find("interval duration") != std::string::npos) {
			userData->distributeEvent->events.back().intervals.back().duration =
					durationToSeconds(std::string(s, len));
		}

	} else if (parent.compare("value") == 0) {
		if (userData->tagBreadCrumbs.find("interval signalPayload") != std::string::npos) {
			userData->distributeEvent->events.back().intervals.back().payload =
					atof(std::string(s,len).c_str());
		}

	} else if (parent.compare("signalType") == 0) {
		userData->distributeEvent->events.back().signalType = std::string(s, len);

	} else if (parent.compare("groupID") == 0) {
		userData->distributeEvent->events.back().targets.groups.push_back(std::string(s, len));

	} else if (parent.compare("resourceID") == 0) {
		userData->distributeEvent->events.back().targets.resources.push_back(std::string(s, len));

	} else if (parent.compare("venID") == 0) {
		userData->distributeEvent->events.back().targets.vendors.push_back(std::string(s, len));

	} else if (parent.compare("partyID") == 0) {
		userData->distributeEvent->events.back().targets.parties.push_back(std::string(s, len));
	}

	return;
}


// ---------------------------------------------------------------------------------------
void XMLCALL endElement(void *data, const char *el) {

	AsXmlParserData *userData = (AsXmlParserData*)data;

	// Update the bread crumbs (remove the element recently closed)
	size_t idx = userData->tagBreadCrumbs.rfind(' ');
	if (idx != std::string::npos) {
		userData->tagBreadCrumbs.erase(idx, std::string::npos);
	}

	return;
}


// ---------------------------------------------------------------------------------------
static inline int toInt(const char *s, int len) {
	char buffer[len + 1];
	memcpy(buffer, s, sizeof(char) * len);
	buffer[len] = '\0';
	return atoi(buffer);
}


// ---------------------------------------------------------------------------------------
static unsigned int hash_str(const char *s) {

	unsigned h = 31;
	static const int A = 54059;
	static const int B = 76963;
	static const int C = 86969;

	while (*s) {
		h = (h * A) ^ (s[0] * B);
		s++;
	}

	return h;
}


// ---------------------------------------------------------------------------------------
static long durationToSeconds(std::string const &strDuration) {

	if (strDuration.substr(0, 2).compare("PT") != 0) {
		// Only supported when the string starts with the 'PT' characteres.
		return -1;
	}

	const char type = strDuration[strDuration.size() - 1];
	int factor;
	if (type == 'S') {
		factor = 1;
	} else if (type == 'H') {
		factor = 3600;
	} else {
		return -1;
	}


	return atol(strDuration.substr(2, strDuration.size() - 2 - 1).c_str()) * factor;
}


// ---------------------------------------------------------------------------------------
inline std::vector<std::string>& splitTokens(std::string const &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}

	return elems;
}


// ---------------------------------------------------------------------------------------
// Converts a string containing a date in XML format into a timestamp value by using the
// module "timestamp.h" provided by GridLAB-D.
static TIMESTAMP xmlDateToTimestampGridlab(std::string const &xml) {

	// 'T' marks the end of the date token and the start of the time token.
	size_t idxEnd = xml.find('T');
	if (idxEnd == std::string::npos) {
		return -1;
	}

	DATETIME dt;
	std::vector<std::string> tokens;

	// Fulfill the fields related to the date.
	{
		std::string xmlDatePart = xml.substr(0, idxEnd);
		splitTokens(xmlDatePart, '-', tokens);
		if (tokens.size() != 3) {
			std::cout << "No hay tres tokens de tipo fecha, sino: " << tokens.size() << std::endl;
			return -1;
		}

		// Complete the items related to the date (year, month and day).
		dt.year = atoi(tokens[0].c_str());
		dt.month = atoi(tokens[1].c_str());
		dt.day = atoi(tokens[2].c_str());
	}

	// Fulfill the fields related to the time.
	{
		// it excludes the final letter ('Z').
		tokens.clear();
		std::string xmlTimePart = xml.substr(idxEnd + 1, xml.size() - idxEnd - 1 - 1);
		splitTokens(xmlTimePart, ':', tokens);
		if (tokens.size() != 3) {
			std::cout << "No hay tres tokens de tipo tiempo, sino: " << tokens.size() << std::endl;
			return -1;
		}

		dt.hour = atoi(tokens[0].c_str());
		dt.minute = atoi(tokens[1].c_str());

		std::string secondMicroseconds = tokens[2];
		tokens.clear();
		splitTokens(secondMicroseconds, '.', tokens);
		dt.second = atoi(tokens[0].c_str());
		if (tokens.size() > 1) {
			dt.microsecond = atoi(tokens[1].c_str());
		}
	}

	dt.tz[0] = 'G'; dt.tz[1] = 'M'; dt.tz[2] = 'T'; dt.tz[3] = '\0';
	dt.is_dst = 1;

//	std::cout << "Year: " << dt.year << std::endl;
//	std::cout << "Month: " << dt.month << std::endl;
//	std::cout << "Day: " << dt.day << std::endl;
//	std::cout << "Hour: " << dt.hour << std::endl;
//	std::cout << "Minute: " << dt.minute << std::endl;
//	std::cout << "Second: " << dt.second << std::endl;
//	std::cout << "Microsecond: " << dt.microsecond << std::endl;

	return callback->time.mkdatetime(&dt);
}


//// ---------------------------------------------------------------------------------------
//// Converts a string containing a date in XML format into a timestamp value.
//static TIMESTAMP xmlDateToTimestamp(std::string const &xml) {
//
//	size_t idxEnd = xml.find('T');
//	if (idxEnd == std::string::npos) {
//		return -1;
//	}
//
//	struct tm tXml;
//	std::vector<std::string> tokens;
//	{
//		std::string xmlDatePart = xml.substr(0, idxEnd);
//		splitTokens(xmlDatePart, '-', tokens);
//		if (tokens.size() != 3) {
//			return -1;
//		}
//
//		tXml.tm_year = atoi(tokens[0].c_str()) - 1900;
//		tXml.tm_mon = atoi(tokens[1].c_str()) - 1;
//		tXml.tm_mday = atoi(tokens[2].c_str());
//	}
//
//	tokens.clear();
//	{
//		// it excludes the final letter ('Z').
//		std::string xmlTimePart = xml.substr(idxEnd + 1, xml.size() - idxEnd - 1 - 1);
//		splitTokens(xmlTimePart, ':', tokens);
//		if (tokens.size() != 3) {
//			return -1;
//		}
//
//		tXml.tm_hour = atoi(tokens[0].c_str());
//		tXml.tm_min = atoi(tokens[1].c_str());
//		tXml.tm_sec = atoi(tokens[2].c_str());
//	}
//
//	return mktime(&tXml);
//}

