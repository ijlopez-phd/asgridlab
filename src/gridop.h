#ifndef _GRIDOP_H_
#define _GRIDOP_H_

#include <string>
#include "agencyservices.h"
#include "gridopservicesproxy.h"
#include "gridlabd.h"

class GridOperator {

public:

	// GridLAB-D standard functions
	GridOperator(MODULE *module);
	~GridOperator();
	int init(OBJECT *parent);
	int create();
	int finalize();
	TIMESTAMP presync(TIMESTAMP t0, TIMESTAMP t1);
	TIMESTAMP sync(TIMESTAMP t0, TIMESTAMP t1);
	TIMESTAMP postsync(TIMESTAMP t0, TIMESTAMP t1);

	GridopServicesProxy* getServicesProxy() const;
	void finish();

public:

	static CLASS *oclass;
	static GridOperator *defaults;

private:
	char1024 wsEndpoint;
	GridopServicesProxy *wsProxy;
};


#endif /* _GRIDOP_H_ */
