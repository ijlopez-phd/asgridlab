#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <wonrest/wonrest.h>
#include "asutils.h"
#include "gridopservicesproxy.h"
#include "gridop.h"
#include "asglobals.h"
#include <time.h>
#include <timestamp.h>

using namespace wonrest;
using namespace std;

CLASS *GridOperator::oclass = NULL;
GridOperator *GridOperator::defaults = NULL;

//static PASSCONFIG passconfig = PC_PRETOPDOWN|PC_BOTTOMUP|PC_POSTTOPDOWN;
static PASSCONFIG passconfig = PC_PRETOPDOWN;
static PASSCONFIG clockpass = PC_PRETOPDOWN;

// Prototypes
void initGlobalVariables();


// -------------------------------------------------------------------------------------
GridOperator::~GridOperator() {

	if (this->wsProxy != NULL) {
		delete this->wsProxy;
	}

	return;
}

// -------------------------------------------------------------------------------------
GridOperator::GridOperator(MODULE *module) : wsProxy(NULL) {

	if (oclass != NULL) {
		gl_warning("GridOperator: This object is already registered.");
		return;
	}

	// Register the class with the module
	oclass = gl_register_class(module, "GridOperator", sizeof(GridOperator), passconfig);
	if (oclass == NULL) {
		GL_THROW("unable to register object class implemented by %s", __FILE__);
		return;
	}

	// Publish the class properties
	if (gl_publish_variable(
			oclass,
			PT_char1024, "servicesEndpoint", PADDR(wsEndpoint), PT_DESCRIPTION, "Url of the services layer of the Grid Operator.",
			NULL) < 1) {
		GL_THROW("unable to publish properties in %s", __FILE__);
	}

	return;
}


// -------------------------------------------------------------------------------------
GridopServicesProxy* GridOperator::getServicesProxy() const {
	return this->wsProxy;
}


// -------------------------------------------------------------------------------------
void GridOperator::finish() {
	this->getServicesProxy()->simulationFinished();
	return;
}

/**
 * -------------------------------------------------------------------------------------------
 * The create function is run before user-defined values are loaded and "init" is performed.
 * This event allows you to set up each "object" prior to user values being set by the GLM file.
 * This is the time to set default values and values used to detect whether the user defined
 * required values (e.g., negative or zero that are not valid).
 *
 * The "create" function must return SUCCESS or FAILED to indicate the result. The simulation
 * will stop if the return value is not SUCCESS.
 *
 */
int GridOperator::create() {
	this->wsEndpoint[0] = '\0';
	this->wsProxy = NULL;

	return SUCCESS;
}


/**
 * -------------------------------------------------------------------------------------------
 * Object initialization is called once after all object have been created.
 * The values specified by the user at the GLM file are loaded at this time.
 *
 */
int GridOperator::init(OBJECT *parent) {

	static bool initialized = false;

	if (initialized == true) {
		return SUCCESS;
	}

	// Check that there is only one GridOperator declared in the GLM.
    FINDLIST *gridOperatorList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "GridOperator", FT_END);
    if (gridOperatorList->hit_count > 1) {
		gl_error("Gridoperator: init: Only one GridOperator object can be declared.");
		throw;
            OBJECT *objGridOperator = gl_find_next(gridOperatorList, NULL);
            GridOperator *gridop = OBJECTDATA(objGridOperator, GridOperator);
            gridop->finish();
    }

    // Get the proxy to access the services layer of the GridOperator.
	this->wsProxy = GridopServicesProxyFactory::getProxy(this->wsEndpoint, scenarioCode);

	// Get the global variables defined within the command line.
	initGlobalVariables();

	// Tell the GridOpeator that the simulation has started.
	idSimulation = this->wsProxy->simulationStarted();

	initialized = true;
	return SUCCESS;
}


/**
 * -------------------------------------------------------------------------------------------
 * Get the global variables defined within the command line.
 *
 */
void initGlobalVariables() {

	char buffer[512];

	GLOBALVAR *globalVar = gl_global_find("df");
	if (globalVar != NULL) {
		if (gl_global_getvar("df", buffer, 512) != NULL) {
			try {
				nDirectories = atoi(buffer);
				gl_output("INFO : [gridop]: [init]: redefined the number of Directory Facilitators: %d", nDirectories);
			} catch (...) {
				gl_error("Error: Failed to convert the global variable 'df' to int.");
			}
		}
	}

	globalVar = gl_global_find("of");
	if (globalVar != NULL) {
		if (gl_global_getvar("of", buffer, 512) != NULL) {
			try {
				overbookingFactor = atof(buffer);
				gl_output("INFO : [gridop]: [init]: redefined the overbooking factor: %f", overbookingFactor);
			} catch (...) {
				gl_error("Error: Failed to convert the global variable 'of' to float.");
			}
		}
	}


	globalVar = gl_global_find("usp");
	if (globalVar != NULL) {
		if (gl_global_getvar("usp", buffer, 512) != NULL) {
			try {
				useStartingPrice = atoi(buffer);
				gl_output("INFO : [gridop]: [init]: Using starting price: %d", useStartingPrice);
			} catch (...) {
				gl_error("Error: Failed to convert the global variable 'usp' to int.");
			}
		}
	}


	globalVar = gl_global_find("rnd");
	if (globalVar != NULL) {
		if (gl_global_getvar("rnd", buffer, 512) != NULL) {
			try {
				useRandom = atoi(buffer);
				gl_output("INFO : [gridop]: [init]: Using randomized sellers: %d", useRandom);
			} catch (...) {
				gl_error("Error: Failed to convert the global variable 'rnd' to int.");
			}
		}
	}


	globalVar = gl_global_find("cf");
	if (globalVar != NULL) {
		if (gl_global_getvar("cf", buffer, 512) != NULL) {
			try {
				constantF = atoi(buffer);
				gl_output("INFO : [gridop]: [init]: Using constant F: %d", constantF);
			} catch (...) {
				gl_error("Error: Failed to convert the global variable 'cf' to int.");
			}
		}
	}

	return;
}


// -------------------------------------------------------------------------------------
int GridOperator::finalize() {
	return SUCCESS;
}


// -------------------------------------------------------------------------------------
TIMESTAMP GridOperator::presync(TIMESTAMP t0, TIMESTAMP t1) {

	TIMESTAMP t2 = t1 + maxStepTime;
	GridopPause pause = this->wsProxy->simulationPaused(t1, (TIMESTAMP)time(NULL));
	TIMESTAMP t2GridOperator = pause.nextPause;
	waitForXmppMessage = pause.eventTriggered;

	return (t2 < t2GridOperator)? t2 : t2GridOperator; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


// -------------------------------------------------------------------------------------
TIMESTAMP GridOperator::sync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2 = TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


// -------------------------------------------------------------------------------------
TIMESTAMP GridOperator::postsync(TIMESTAMP t0, TIMESTAMP t1) {
	TIMESTAMP t2= TS_NEVER;
	return t2; // return t2>t1 on success, t2=t1 for retry, t2<t1 on failure
}


//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION OF CORE LINKAGE
//////////////////////////////////////////////////////////////////////////

EXPORT int create_GridOperator(OBJECT **obj)
{
	try
	{
		*obj = gl_create_object(GridOperator::oclass);
		if (*obj!=NULL)
			return OBJECTDATA(*obj,GridOperator)->create();
	}
	catch (char *msg)
	{
		gl_error("create_GridOperator: %s", msg);
	}
	return 0;
}

EXPORT int init_GridOperator(OBJECT *obj, OBJECT *parent)
{
	try
	{
		if (obj!=NULL) {
			return OBJECTDATA(obj, GridOperator)->init(parent);
		}
	} catch (char *msg)	{
		gl_error("init_GridOperator(obj=%d;%s): %s", obj->id, obj->name?obj->name:"unnamed", msg);
	}
	return 0;
}

EXPORT TIMESTAMP sync_GridOperator(OBJECT *obj, TIMESTAMP t1, PASSCONFIG pass)
{
	TIMESTAMP t2 = TS_NEVER;
	GridOperator *my = OBJECTDATA(obj,GridOperator);
	try
	{
		switch (pass) {
		case PC_PRETOPDOWN:
			t2 = my->presync(obj->clock,t1);
			break;
		case PC_BOTTOMUP:
			t2 = my->sync(obj->clock,t1);
			break;
		case PC_POSTTOPDOWN:
			t2 = my->postsync(obj->clock,t1);
			break;
		default:
			GL_THROW("invalid pass request (%d)", pass);
			break;
		}
		if (pass==clockpass)
			obj->clock = t1;
		return t2;
	}
	catch (char *msg)
	{
		gl_error("sync_GridOperator(obj=%d;%s): %s", obj->id, obj->name?obj->name:"unnamed", msg);
	}
	return TS_INVALID;
}
