
#ifndef _ASUTILS_H_
#define _ASUTILS_H_

#include <string>
#include <vector>
#include "timestamp.h"





using namespace std;

class AsUtils {

public:

	static string uriEncode(const string &src);
	static string uriDecode(const string &src);
	static string formatTimestamp(TIMESTAMP timestamp);
	static vector<string>& split(string const &s, char delim, vector<string>& elems);


private:

};


#endif /* _ASUTILS_H_ */
