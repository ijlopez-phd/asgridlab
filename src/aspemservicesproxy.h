#ifndef ASPEMSERVICESPROXY_H_
#define ASPEMSERVICESPROXY_H_

#include <timestamp.h>
#include <string>
#include <asbox.h>



using namespace std;

class AspemServicesProxy {

public:

	/**
	 * Destructor.
	 */
	virtual ~AspemServicesProxy() = 0;


	/**
	 * Tells the ASPEM that a new simulation is starting.
	 */
	virtual void simulationStarted() = 0;


	/**
	 * Registers a new AsBox with the ASPEM.
	 */
	virtual void addAsBox(string code, UserPreferences *userPrefs) = 0;


	/**
	 * Tells the ASPEM that the simulation is over.
	 */
	virtual void simulationFinished() = 0;
};


class AspemRestfulProxy : public AspemServicesProxy {

public:

	AspemRestfulProxy(string wsEndpoint, int idSimulation);

	virtual ~AspemRestfulProxy();

	virtual void simulationStarted();

	virtual void addAsBox(string code, UserPreferences *userPrefs);

	virtual void simulationFinished();

private:

	int idSimulation;
	string wsEndpoint;
};


class AspemMockProxy : public AspemServicesProxy {

public:

	AspemMockProxy() {}

	virtual ~AspemMockProxy() {}

	virtual void simulationStarted() {}

	virtual void addAsBox(string code, UserPreferences *userPrefs) {}

	virtual void simulationFinished() {}
};


class AspemServicesProxyFactory {
public:

	static AspemServicesProxy* getProxy(string wsEndpoint, int idSimulation);
};

#endif /* ASPEMSERVICESPROXY_H_ */
