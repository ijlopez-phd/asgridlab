
# asGridLabD #

## Description ##

_asGridLabD_ is a module that aims at integrating the Agency Services concept into the GridLAB-D simulator. It is part of the PhD thesis of **Ignacio Lopez-Rodriguez** at the [Institute for Intelligent Systems and Numeric Applications (SIANI)](http://www.siani.es), [University of Las Palmas de Gran Canaria](http://www.ulpgc.es).

Agency Services are software agents deployed in the cloud that users can contract in order to automate their participation in virtual communities without losing intelligence or autonomy. Virtual communities that may benefit from the participation of autonomous agents are those that run any type of bargain processes and require continuous attention from users. A full description of the Agency Services can be found in the following the papers:

* _Agency Services_, on the _International Conference on Agents and Artificial Intelligence_ (ICAART), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2010.  

* _Software Agents as Cloud Computing Services_, on Springer _Advances on Practical Applications of Agents and Multiagent Systems_ (PAAMS), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2011.


The Smart Grid is the next generation of the Power Grid. It is envisioned as a great virtual organization in which consumers and producers negotiate continuously the energy to be generated and demanded. In this scenario, a solution based on the Agency Services would bring to the users the possibility to negotiate dynamically the production and consumption of energy blocks according to their interests. A more detailed description of this approach can be found in the following papers:

* _Agent-Based Services for Building Markets in Distributed Energy Environments_, on the _International Conference on Renewable Energies and Power Quality_ (ICREPQ), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2010.  

* _Challenges of using smart local devices for the management of the Smart Grid_, on the _IEEE International Conference on Smart Grid Communications_ (SmartGridComm), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2011.


GridLab-D is a power grid simulator that is widley used in the research community. The goal of the module _asGridLabD_ is to communicate GridLab-D with the project [EnergyAgents](https://github.com/ignux02/energyagents), which in turn simulates a layer of broker agents deployed in the cloud.


## License ##

_asGridLabD_ is free software: you can redistribute it and/or modify it under the terms of the [**GNU Lesser General Public License**](http://www.gnu.org/licenses/lgpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

In short, the  _asGridLabD_ module guarantees the four essential freedoms of Free Software, **and** it can be linked by proprietary software.

asGridLabD is copyright (c) 2013 Ignacio Lopez-Rodriguez.


## Technologies ##

_asGridLabD_ is developed in C++. Moreover, the following resources are used:

* [GNU Autotools](http://www.gnu.org/software/autoconf/) for compiling and installing the module.

* [Gloox](http://camaya.net/gloox/) for establishing XMPP communications.

* [Expat](http://expat.sourceforge.net/) for parsing XML files.

* [Wonrest](https://github.com/ignux02/wonrest) for accessing RESTful services.
