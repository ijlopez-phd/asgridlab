#!/usr/bin/python

import sys
import random
from optparse import OptionParser


def get_schedules_filename(profile):
    filename = 'sch_producer.glm'
    if profile == '0':
        filename = 'sch_producer.glm'
    elif profile == '1':
        filename = 'sch_producer-less-1.glm'
    elif profile == '2':
        filename = 'sch_producer-less-2.glm'
    elif profile == '3':
        filename = 'sch_producer-less-3.glm'
    elif profile == '4':
        filename = 'sch_producer-less-4.glm'

    return filename


def load_schedules(profile):
    filename = get_schedules_filename(profile) 

    sch_file = open(filename, 'r')
    easy_schedules = []
    hard_schedules = []
    normal_schedules = []
    for line in sch_file:
        s_line = line.strip()
        if s_line.startswith('schedule '):
            # This line contains the beginning of new schedule definition.
            # - Get the name of the schedule
            tokens = s_line.split(' ')
            assert (len(tokens) == 3), "(E!) Bad schedule definition."
            if ('easy' in tokens[1]):
                easy_schedules.append(tokens[1])
            elif ('hard' in tokens[1]):
                hard_schedules.append(tokens[1])
            elif ('normal' in tokens[1]):
                normal_schedules.append(tokens[1])
            elif ('mixed' in tokens[1]):
                easy_schedules.append(tokens[1])
                hard_schedules.append(tokens[1])
    
    return (normal_schedules, easy_schedules, hard_schedules, normal_schedules + easy_schedules + hard_schedules)


def pick_schedule(schedules_sets):
    all_schedules = schedules_sets[3]
    return random.choice(all_schedules)


#def pick_schedule(schedules_sets):
#
#    sch_type = random.randint(0, 4)
#    if (sch_type in [0, 2]):
#        # Hard load.
#        schedules = schedules_sets[2]
#    elif (sch_type in [1, 3]):
#        # Easy load.
#        schedules = schedules_sets[1]
#    else:
#        # Normal load.
#        schedules = schedules_sets[0]
#
#    return random.choice(schedules)


        

"""

    Script.

"""

# Description of the arguments
usage = "usage: %prog [options] glm_file"
parser = OptionParser(usage = usage)
parser.add_option("-o", "--output", dest="file", default="out.glm", help="Output GLM file.")
parser.add_option("-l", "--level", dest="level", default="0", help="Default OADR level.")
parser.add_option("-d", "--data", dest="data_path", default="./.data", help="Path for the output data files.")
parser.add_option("-s", "--suffix", dest="suffix", default="", help="Suffix added to the data files names of houses.")
parser.add_option("-x", "--xmpp", dest="xmpp_url", default="localhost", help="Url of the XMPP server.")
parser.add_option("-t", "--step", dest="step_time", default="60", help="Elapsed time (in seconds) betweeen the logged samples.")
parser.add_option("-r", "--record", dest="record", default="0", help="0, use recorders; 1, use collectors; 2, use both")
parser.add_option("--set-auction", dest="auction", default="0", help="0, do not set for auctions; 1, set for auctions")
parser.add_option("--pmin", dest="price_min", default="80", help="Minimum price")
parser.add_option("--pmax", dest="price_max", default="150", help="Maximum price")
parser.add_option("--profile", dest="profile", default="0", help="Auction nodes' profile: 0, producers; 1, consumers; 2, mixed.")

(options, args) = parser.parse_args()

# Check if the arguments of the invoking command is valid. 
if len(args) < 1:
    print "Syntax is not valid. Try the option '--help' for more information."
    sys.exit(1)
elif options.level not in ['0', '1', '2', '3']:
    print "Syntax is not valid. Allowed values for the argument 'level' are 0, 1, 2 or 3."
    sys.exit(1)
    

in_glm, out_glm = args[-1], options.file
level = options.level
data_path = options.data_path

MODULE_BLOCK = """
module agencyservices {{

#define as_data_path={data_path}
#define as_level={level}

#if as_level==0
#define as_dr_mode=normal
#else
#if as_level==1
#define as_dr_mode=moderate
#else
#if as_level==2
#define as_dr_mode=high
#else
#if as_level==3
#define as_dr_mode=critical
#else
#define as_dr_mode=warning
#endif
#endif
#endif
#endif

    nodeName "ieee13";
    scenarioCode "ieee13";
    maxStepTime 7200;
    xmppUrl "{xmppUrl}";
    oadrLevel ${{as_level}};
}};
"""

GRIDOP_BLOCK_OPEN = """
object GridOperator {
    name gridop;
    servicesEndpoint "localhost:9090/gridop/resources";
"""

GRIDOP_BLOCK_CLOSE = """
};\n
"""

ASPEM_BLOCK = """
    object ASPEM {
        name aspem01;
        longName aspem01;
        parent gridop;
        servicesEndpoint "localhost:8080/aspem/resources";
    };\n
"""


ASBOX_BLOCK = """
     object ASBox {{
          name {asboxName};
          aspem {aspemName};
          parent {houseName};
          levelsSchedule {levelsSchedule};
     }};\n
"""


ASBOX_BLOCK_AUCTION = """
     object ASBox {{
          name {asboxName};
          aspem {aspemName};
          parent {houseName};
          levelPrice1 {levelPrice1};
          levelPrice2 {levelPrice2};
          levelPrice3 {levelPrice3};
          levelsSchedule {levelsSchedule};
          startingPrice {startingPrice};
     }};\n
"""


RECORDER_BLOCK = """
     object recorder {{
         parent {houseName};
         file ${{as_data_path}}/{recorderName}.csv;
         property total_load;
         interval {interval};
         limit 0;
     }};\n
"""

COLLECTORS_BLOCKS = """
object collector {
    file ${as_data_path}/houses-load-${as_dr_mode}.csv;
    group "class=house";
    property sum(total_load),avg(total_load);
    interval 0;
    limit 0;
};

object collector {
    file ${as_data_path}/waterheaters-load-${as_dr_mode}.csv;
    group "class=waterheater";
    property sum(actual_load),avg(actual_load);
    interval 0;
    limit 0;
};

object collector {
    file ${as_data_path}/hvacs-load-${as_dr_mode}.csv;
    group "class=house";
    property sum(hvac_load),avg(hvac_load);
    interval 0;
    limit 0;
};

object collector {
    file ${as_data_path}/zips-load-${as_dr_mode}.csv;
    group "class=ZIPload";
    property sum(actual_power.real);
    interval 0;
    limit 0;
};
"""

INCLUDES_AUCTION = """
#include "{filename}"\n
"""


if options.auction == "1":
    price_section_size = (int(options.price_max) - int(options.price_min)) / 3
    price_bound_1 = int(options.price_min) + price_section_size
    price_bound_2 = price_bound_1 + price_section_size
    random.seed(None)



in_f = open(in_glm, 'r')
out_f = open(out_glm, 'w')

schedules_sets = load_schedules(options.profile)
first_object, indent_level, block = False, 0, ''
asbox_name, house_name, aspem_name = '', '', 'aspem01'
suffix = options.suffix.strip()
if len(suffix) > 0:
    suffix = '_' + suffix

for line in in_f:

    s_line = line.strip()

    # Update the level of objects indentation
    if ('object' in s_line) and ('{' in s_line):
        indent_level += 1
    elif (indent_level > 0) and ((s_line == '}') or (s_line == '};')):
        indent_level -= 1


    # What to write
    if (s_line == '}') or (s_line == '};'): # End of block
        if (block == 'clock'):
            # End of the clock block.
            # Below goes the declaration of the agency services module.
            out_f.write(line)
            out_f.write(MODULE_BLOCK.format(level = options.level, data_path = options.data_path, xmppUrl = options.xmpp_url))
            out_f.write(INCLUDES_AUCTION.format(filename = get_schedules_filename(options.profile)))

            block = ''

        elif (block == 'house') and (indent_level == 0):
            # End of the house block.
            # Below goes the declaration of the ASBox and the Recorder (before closing the house object)
            if options.auction == "0":
                levels_schedule = pick_schedule(schedules_sets)
                out_f.write(ASBOX_BLOCK.format(asboxName = asbox_name, houseName = house_name, aspemName = aspem_name, levelsSchedule = levels_schedule))
            else:
                level_price_1 = random.randint(int(options.price_min), price_bound_1)
                level_price_2 = random.randint(level_price_1 + 1, price_bound_2)
                level_price_3 = random.randint(level_price_2 + 1, int(options.price_max))
                levels_schedule = pick_schedule(schedules_sets)
                starting_price = level_price_2 + (0.15 * level_price_2) 
                out_f.write(ASBOX_BLOCK_AUCTION.format(asboxName = asbox_name, houseName = house_name, aspemName = aspem_name, levelPrice1 = level_price_1, levelPrice2 = level_price_2, levelPrice3 = level_price_3, levelsSchedule = levels_schedule, startingPrice = starting_price))


            if (options.record == '0') or (options.record == '2'):
                out_f.write(RECORDER_BLOCK.format(houseName = house_name, recorderName = asbox_name + suffix, interval = options.step_time, data_path = options.data_path))

            out_f.write(line)
            block = ''
            house_name = ''
            asbox_name = ''

        else:
            out_f.write(line)

    elif s_line.startswith('object') and (first_object == False):
        # Before the first object the declaration of the Gridop and the ASPEM are inserted.
        if (options.record == '1') or (options.record == '2'):
            out_f.write(COLLECTORS_BLOCKS)
        out_f.write(GRIDOP_BLOCK_OPEN)
        out_f.write(ASPEM_BLOCK)
        out_f.write(GRIDOP_BLOCK_CLOSE)
        out_f.write(line + '\n')
        first_object = True
    else:
        # Copy into the output file the line read from the input file
        out_f.write(line)



    # Just in case, get the name of the house and build the asbox's name
    if (indent_level == 1) and (s_line.startswith('name ')) and (block == 'house'):
        house_name = s_line[5:len(s_line) - 1]
        tokens = house_name.split('_')
        asbox_name = 'i' + tokens[0][5:] + tokens[4]
        
    # Context
    if s_line.startswith('clock {'):
        block = 'clock'
    elif s_line.startswith('object house {'):
        block = 'house'

in_f.close()
out_f.close()

sys.exit(0)

# end of the script
