#!/usr/bin/python

import sys
from optparse import OptionParser
from subprocess import call

usage = "usage: %prog [options] glm_file"
parser = OptionParser(usage = usage)
parser.add_option("-n", "--nsims", dest="n_sims", default="1", help="Number of simulations per level.")
parser.add_option("-s", "--start", dest="start", default="0", help="Number of the first iteration.")
parser.add_option("-d", "--datapath", dest="data_path", default=".data", help="Path of the output files.")
parser.add_option("-x", "--xmpp", dest="xmpp_url", default="localhost", help="Url of the XMPP server.")
parser.add_option("-t", "--step", dest="step_time", default="7200", help="Elapsed time (in seconds) between the logged samples.")

(options, args) = parser.parse_args()

if len(args) < 1:
    print "Invalid arguments. Try the option '--help' for more information."
    sys.exit(1)

N_START = int(options.start)
for n_sim in range(int(options.n_sims)):
    for n_level in range(4): # levels 0, 1, 2 and 3.
        batch_result = call(["fulfill.py",
              "-l", str(n_level),
              "-d", options.data_path,
              "-t", options.step_time,
              "-s", "p%d_t%s_n%d" % (n_level, options.step_time, n_sim + N_START),
              "-x", options.xmpp_url,
              "-o", ".batchout.glm",
              args[-1]])

        if batch_result != 0:
            print "Error: failed to call the script 'fulfill.py'."
            sys.exit(1)
            
        batch_result = call(["gridlabd", ".batchout.glm", "--define", "useWsMock"])

        if batch_result != 0:
            print "Error: failed to call the program 'gridlabd'."
            sys.exit(1)

sys.exit(0)
