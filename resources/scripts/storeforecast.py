#!/usr/bin/python

import os
import sys
import csv
from datetime import datetime
from glob import glob
from optparse import OptionParser
import psycopg2


def connect_db(table_name):
    """ Connect to the database and create the table if it didn't exist.""" 

    conn = None
    try:
        conn = psycopg2.connect(
                    database = "agencyservices",
                    host = "localhost",
                    user = "agencyservices",
                    password = "agencyservices")
    except Exception, e:
        None

    drop_sql = """
        DROP TABLE IF EXISTS {name} CASCADE;
    """

    create_sql = """
        CREATE TABLE {name} (
            ID SERIAL PRIMARY KEY,
            ASBOX VARCHAR(512) NOT NULL,
            PROFILE SMALLINT NOT NULL CHECK ((PROFILE >= 0) and (PROFILE <=3)),
            TIMESTAMP TIMESTAMP WITHOUT TIME ZONE NOT NULL,
            LOAD DECIMAL NOT NULL CHECK (LOAD >= 0));

        CREATE INDEX idx_{name}_asbox ON {name}(asbox);

        CREATE INDEX idx_{name}_profile ON {name}(profile);
    """

#    clear_sql = """
#        DELETE FROM {name};
#    """

    cur = conn.cursor()
    cur.execute(drop_sql.format(name = table_name))
    cur.execute(create_sql.format(name = table_name))
#    cur.execute(clear_sql.format(name = table_name))
    conn.commit()
    cur.close()
        
    return conn


def close_db(conn):
    """ Close the connection to the database."""
    try:
        conn.close()
    except Exception, e:
        None

    return

def insert_db(conn, table_name, n_profile, data):
    """ Insert into the database all data contained in the dictionary structure.
        The data is related to a profile."""
    

    insert_sql = """
        INSERT INTO {name} (asbox, profile, timestamp, load) 
        VALUES (%s, %s, %s, %s);
    """

    cur = conn.cursor()
    for key, value in data.iteritems():
        cur.execute(
            insert_sql.format(name = table_name),
            (key[0], n_profile, key[1], value[0] / float(value[1])))

    conn.commit()
    cur.close()
    
    return None



"""
    Script.
"""

usage = "usage: %prog [options] data_path"
parser = OptionParser(usage = usage)
parser.add_option("-c", "--code", dest="code", help="Code name of the scenario.")
(options, args) = parser.parse_args()

if len(args) < 1:
    print "Error: Invalid call: Required arguments are missing. Try with '--help'."
    sys.exit(1)


# Check if the data folder exists. 
data_path = os.path.abspath(args[-1])
if not os.path.isdir(data_path):
    print "Error: The specified path is not a valid folder. Please check that the path corresponds to a readable folder of the filesystem."
    sys.exit(1)
elif (options.code is None) or (len(options.code) == 0):
    print "Error: A code name must be provided. Try the option '--help' for more information."
    sys.exit(1)


# Connect to the database
db_conn = connect_db(options.code) 
if db_conn == None:
    print "Error: Failed to connect to the database."
    sys.exit(1)



N_COLS = 2
IDX_DATE_TIME = 0
IDX_LOAD = 1
time_zones = (' PDT', ' GMT', ' UTC', ' EST', ' CST')
for n_profile in range(4):
    data = {}
    for csv_path in glob(data_path + '/*.csv'):

        # Check if the file corresponds to the profile being processed.
        (path, file_name) = os.path.split(csv_path)
        if ("_p%d_" % (n_profile)) not in file_name:
            continue

        # strip the extension from the file name.
        file_name = file_name[:file_name.index('.')]
        tokens = file_name.split('_')
        if len(tokens) != 4:
            print "Warning: The file '%s' was not processed." % file_name 
            continue

        house_name = tokens[0]

        # The file corresponds to the profile being processed. 
        # Its data is stored in the data dictionary.
        with open(csv_path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:

                if row[0].strip().startswith('#'):
                    # This line is a comment.
                    continue
                elif len(row) != N_COLS:
                    # Warning: This row has not the expected number of columns.
                    continue


                # This row must be treated.
                #  - Strip the time zone name if it is present at the end of the string.
                dt = row[IDX_DATE_TIME]
                if dt.endswith(time_zones):
                    dt = dt[:-4]
                    
                # Store the value in the dictionary.
                load = float(row[IDX_LOAD])
                key = (house_name, str(datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')))
                value = data.get(key, None)
                if value == None:
                    data[key] = [load, 1]
                else:
                    data[key] = [value[0] + load, value[1] + 1]

    # Store the dictionary into the database
    insert_db(db_conn, options.code, n_profile, data)
    data.clear()

close_db(db_conn)
sys.exit(0)




