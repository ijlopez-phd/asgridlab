#!/usr/bin/python

"""

    Script.

"""


import sys
import argparse

def printReport(directories, overbookers, auc, pretty=True, df=False):
    # Calculate the totals.
    total_demanded = 0.0
    total_exchanged = 0.0 
    total_offered = 0.0
    total_cost = 0.0
    n_nodes = 0
    for df_name,df_data in directories.iteritems():
        n_nodes += df_data['n_nodes']
        total_demanded += df_data['demanded']
        total_offered += df_data['offered']
        total_exchanged += df_data['covered']
        total_cost += df_data['cost']

    # Calculate data related to the overbookers. 
    n_overbookers = len(overbookers)
    ov_factors = []
    overbooking_mean = 0
    for key, value in overbookers.iteritems():
        ov_factor = (value[0] / value[1]) * 100
        ov_factors.append(ov_factor)
        overbooking_mean += ov_factor
    del overbookers
    if n_overbookers > 0:
        overbooking_mean /= n_overbookers
    ov_factors.sort(reverse=True)


    if not pretty:
        # Print the report in the simple format
        output = "{n_nodes}, {n_cons}, {n_prods}, {n_dfs}, {n_lp}, {n_lc}, {n_ea}, {n_ca}, {n_ov}, {ov_mean}, {exchanged}, {demanded}, {offered}, {cost_mean}".format(n_nodes = n_nodes, n_cons = sum(auc['n_consumers']), n_prods = sum(auc['n_producers']), n_dfs = len(directories), n_lp = sum(auc['lost_producers']), n_lc = sum(auc['lost_consumers']), n_ea = sum(auc['empty_auctions']), n_ca = sum(auc['cancelled_auctions']), n_ov = n_overbookers, ov_mean = "{0:.2f}".format(overbooking_mean), exchanged = "{0:.2f}".format(total_exchanged), demanded = "{0:.2f}".format(total_demanded), offered = "{0:.2f}".format(total_offered), cost_mean = "{0:.2f}".format(total_cost / total_exchanged))
        for df_name, df_data in directories.iteritems():
            str_df = ",{exchanged};{demanded};{offered};{cost};{cost_mean}".format(exchanged = "{0:.2f}".format(df_data['covered']), demanded = "{0:.2f}".format(df_data['demanded']), offered= "{0:.2f}".format(df_data['offered']), cost = "{0:.2f}".format(df_data['cost']), cost_mean = "{0:.2f}".format(df_data['cost'] / df_data['covered']))
            output += str_df

        print output
        return

    # Print the report in the pretty format 
    print "\n\n"
    print "  ----------------------------------------------------------\n"
    print "    N nodes: ", n_nodes, "\n"
    print "    N consumers: ", sum(auc['n_consumers']), auc['n_consumers'], "\n"
    print "    N producers: ", sum(auc['n_producers']), auc['n_producers'], "\n"
    print "    Exchanged: ", "{0:.3f}".format(total_exchanged), "\n"
    print "    Demanded: ", "{0:.3f}".format(total_demanded), "\n"
    print "    Offered: ",  "{0:.3f}".format(total_offered), "\n"
    if total_demanded > 0:
        print "    Covered:  ", "{0:.2f}".format((total_exchanged/total_demanded)*100) + "%\n"
    else:
        print "    Covered:  ", "{0:.2f}".format(0) + "%\n"

    if total_offered > 0:
        print "    Provided: ", "{0:.2f}".format((total_exchanged/total_offered)*100) + "%\n"
    else:
        print "    Provided: ", "{0:.2f}".format(0) + "%\n"

    print "    Cost: ", "{0:.3f}".format(total_cost), "\n" 
    if total_exchanged > 0:
        print "    Unit cost: ", "{0:.3f}".format(total_cost / total_exchanged), "\n" 
    else:
        print "    Unit cost: 0\n"
 
    print "    Empty auctions: ", sum(auc['empty_auctions']), auc['empty_auctions'], "\n"
    print "    Cancelled auctions: ", sum(auc['cancelled_auctions']), auc['cancelled_auctions'], "\n"
    print "    Lost consumers: ", sum(lost_consumers), lost_consumers, "\n"
    print "    Lost producers: ", sum(auc['lost_producers']), auc['lost_producers'], "\n"
    print "    Overbookers: ", n_overbookers, " (" + "{0:.2f}".format((n_overbookers/n_nodes)*100) + "%)\n"
    print "    Overbooking mean: ", "{0:.2f}".format(overbooking_mean) + "%\n"
    for ov_factor in ov_factors[0:5]:
        print "       -", "{0:.2f}".format(ov_factor)
    
    if df == True:
        print "\n"
        print "    Directories:"
        for df_name,df_data in directories.iteritems():
            print "        + " + df_name + ":"
            print "            - N nodes: ", df_data['n_nodes'], "\n" 
            print "            - Exchanged: ", df_data['covered'], "\n"
            print "            - Demanded: ", "{0:.3f}".format(df_data['demanded']), "\n" 
            print "            - Offered: ", "{0:.3f}".format(df_data['offered']), "\n"
            print "            - Covered: ", "{0:.2f}".format((df_data['covered']/df_data['demanded'])*100) + "%\n"
            print "            - Provided: ", "{0:.2f}".format((df_data['covered']/df_data['offered'])*100) + "%\n"
            print "            - Cost: ", "{0:.3f}".format(df_data['cost']), "\n" 
            if df_data['covered'] > 0:
                print "            - Unit cost: ", "{0:.3f}".format(df_data['cost'] / df_data['covered']), "\n" 
            else:
                print "            - Unit cost: 0\n"
    print "  ----------------------------------------------------------\n"
    print "\n"
    

parser = argparse.ArgumentParser(description="Script for building a simulation report from the CSV log file created by the simulation.")
parser.add_argument("csv_path", help="Path to the CSV file to be parsed.")
parser.add_argument("--pretty", action="store_true")
parser.add_argument("--df", action="store_true")
args = vars(parser.parse_args())

input_file = open(args['csv_path'], 'r')

epsilon = 0.5
n_nodes = 0
total_demanded = 0
total_covered = 0
total_offered = 0
total_provided = 0
total_cost = 0
overbookers = {}
directories = {}
auction_data = {}
lost_consumers = []
lost_producers = []
empty_auctions = []
cancelled_auctions = []
n_consumers = []
n_producers = []
for line in input_file:

    line = line.strip()
    if (len(line) == 0) or (line[0] == '#'):
        continue

    tokens = line.split(',')

    node = tokens[0]

    df_name = tokens[1]
    df_data = directories.get(df_name)
    if df_data == None:
        df_data = dict(n_nodes=0, covered=0.0, demanded=0.0, offered=0.0, cost=0.0)

    if len(lost_consumers) == 0:
        n_blocks = len(tokens) - 2
        lost_consumers = [0] * n_blocks 
        lost_producers = [0] * n_blocks 
        cancelled_auctions = [0] * n_blocks 
        empty_auctions = [0] * n_blocks 
        n_consumers = [0] * n_blocks 
        n_producers = [0] * n_blocks 

    n_block = -1
    for item in tokens[2:]:
        n_block += 1
        item = item.strip()
        if item.startswith('-/'):
            # In this block the client is not participating in the auction.
            continue

        idx_sep_1 = item.index('/')
        idx_sep_2 = item.index('/', idx_sep_1 + 1)
        if item.startswith('C-'):
            # The client is participating as consumer.
            n_consumers[n_block] += 1
            covered = float(item[2:idx_sep_1])
            demanded = float(item[idx_sep_1 + 1:idx_sep_2])
            total_demanded += demanded 
            df_data['demanded'] += demanded 
            if (demanded - covered) < epsilon:
                total_covered += covered 
                df_data['covered'] += covered 
            elif covered > 0:
                cancelled_auctions[n_block] += 1


            if (demanded > 0) and (((demanded - covered) > epsilon) or (covered == 0)):
                lost_consumers[n_block] += 1

            if covered == 0:
                empty_auctions[n_block] += 1


            prices_list = item[item.index('#') + 1:]
            if len(prices_list) > 0:
                for price_item in prices_list.split(';'):
                    price_tokens = price_item.split('-')
                    assert (len(price_tokens) == 2) or (len(price_tokens) == 3), line
                    if len(price_tokens) == 3:
                        price_tokens[0] = 0
                        price_tokens[1] = price_tokens[2]
                    cost = float(price_tokens[0]) * float(price_tokens[1])
                    total_cost += cost 
                    df_data['cost'] += cost 

        elif item.startswith('P-'):
            # The client is participating as producer.
            n_producers[n_block] += 1
            provided = float(item[2:idx_sep_1])
            offered = float(item[idx_sep_1 + 1:idx_sep_2])
            df_data['offered'] += float(item[idx_sep_1 + 1:idx_sep_2])
            if (provided - offered) > 0.1:
                overbookers[node] = (provided, offered)
            total_provided += provided
            total_offered += offered 

            if (offered > 0) and (provided == 0.0):
                lost_producers[n_block] += 1

    df_data['n_nodes'] += 1
    n_nodes += 1
    directories[df_name] = df_data

auction_data = { 'lost_consumers' : lost_consumers, 'lost_producers': lost_producers, \
                 'cancelled_auctions' : cancelled_auctions, 'empty_auctions': empty_auctions, \
                 'n_consumers' : n_consumers, 'n_producers' : n_producers}

printReport(directories, overbookers, auction_data, args['pretty'], args['df'])
