
# Shapers #

Shapers are a variant of player.

- Schedules (using POSIX *cron* standard).
- Amplitude: copies value directly.
- Pulse-width: on/off width a probability.
- Queues: accrues to threshold before "on".

