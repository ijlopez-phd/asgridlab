
# Players #

Used to update information at a specific time from a file.

- Players can alter object properties as a function of time.
- Used to alter boundary conditions
- Have a sense of time and can be looped.

Tapes apply a time-series to a single property of the parent object (the parent object is the object in which is declared the Player object, and to which it is related).

Several possible sources of tape data:

- File: source is a CSV file.
- ODBC: source is a database.
- Memory: source is a global variable.

## Property ##

The field "property" makes reference to the parent's property that is updated.

## Loop ##

Indicates how many times the data source is to be read.

- 
